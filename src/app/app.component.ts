import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

// import { HomePage } from '../pages/home/home';
import { LoginPage } from '../pages/login/login';
import { MenuPage } from '../pages/menu/menu'
import { HomePage } from '../pages/home/home'
import { Http, Headers, RequestOptions } from '@angular/http'
import { QuestionnairePage } from '../pages/questionnaire/questionnaire'
// import { AppsetProvider } from './../providers/appset/appset';
import { InterviewsPage } from '../pages/interviews/interviews'
import * as firebase from 'firebase';
import { AnnouncelistPage } from '../pages/announcelist/announcelist';
import { AnnounceviewPage } from '../pages/announceview/announceview';
import { VideoPage } from '../pages/video/video';
import { NotificationPage } from '../pages/notification/notification';
import { SurvayPage } from "../pages/survay/survay";
import { TestPage } from "../pages/test/test";

import { EventPage } from '../pages/event/event';
import { ContactPage } from '../pages/contact/contact';
import { DtsPage } from '../pages/dts/dts';
import { CameraPage } from '../pages/camera/camera';
import { GpaxPage } from '../pages/gpax/gpax';

import {FirebaseProvider} from '../providers/firebase/firebase'
import { AdvisorDisPage } from '../pages/advisor-dis/advisor-dis';
import { Network } from '@ionic-native/network';
import { OneSignal } from '@ionic-native/onesignal';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage: any;
  userId: any;
  constructor(platform: Platform,
    statusBar: StatusBar,
    splashScreen: SplashScreen,
    public http: Http,
    public db:FirebaseProvider,
    public network:Network,
    public oneSignal:OneSignal

  ) {

    platform.ready().then(() => {
      statusBar.styleDefault();
      splashScreen.hide();

        this.oneSignal.startInit("123485eb-918f-4b28-b1cf-655c9355d386")
        this.oneSignal.setSubscription(true)
        this.oneSignal.inFocusDisplaying(this.oneSignal.OSInFocusDisplayOption.Notification)
        this.oneSignal.handleNotificationReceived().subscribe((res) => {
  
  })

  this.oneSignal.endInit()
    });

    let backAction =  platform.registerBackButtonAction(() => {
      console.log("second");
      
      backAction();
    },2)

    this.loginauto()

  }

  loginauto() {
        let disconnectSubscription = this.network.onDisconnect().subscribe(() => {
      this.db.internetlost()
  });
  let connectSubscription = this.network.onConnect().subscribe(() => {
        this.db.stopLoading()

  });  

    this.userId = localStorage.getItem('UserId')
    if (!localStorage.getItem('fist_user')) {
      this.rootPage = InterviewsPage
    } else {

      if (this.userId == '' || this.userId == null || this.userId == undefined) {

        this.rootPage = LoginPage;
      } else {
        this.rootPage = HomePage;
       
      }
    }


  }


}

