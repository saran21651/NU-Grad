import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { CameraPage } from '../pages/camera/camera'


import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { LoginPage } from '../pages/login/login';

// modal
import { ModalInfoPage } from '../pages/modal-info/modal-info';
import { ModalcalendarPage } from '../pages/modal-calendar/modal-calendar';
import { AddEventPage } from '../pages/modal-calendar-add/modal-calendar-add';
import { Modalnotification } from '../pages/modal-notification/modal-notification';
import { Modalcapital } from '../pages/modal-capital/modal-capital';
import { Modaltalk } from '../pages/modal-talk/modal-talk';
import { Modalqrcode } from '../pages/modal-qrcode/modal-qrcode';

// book
import { BookService } from '../pages/book-service/book-service';
import { BookBuy } from '../pages/book-buy/book-buy';
import { BookNews } from '../pages/book-news/book-news';
import { BookContact } from '../pages/book-contact/book-contact';

//manage
import { ManageAlumni } from '../pages/manage-alumni/manage-alumni';
import { ManageNotifiction } from '../pages/manage-notification/manage-notification';
//media
import { MediaKnow } from '../pages/media-know/media-know';
import { MediaNotifiction } from '../pages/media-notification/media-notification';

//research
import { ResearchCapital } from '../pages/research-capital/research-capital';
import { ResearchPaper } from '../pages/research-paper/research-paper';

import { CalendarModule } from 'ionic3-calendar-en';

import { Calendar } from '@ionic-native/calendar';

import { CapitalPage } from '../pages/capital/capital';

import { BookServiceDetailPage } from '../pages/book-service-detail/book-service-detail';
import { BookOtherbookDetailPage } from '../pages/book-otherbook-detail/book-otherbook-detail'
import { ReseachPaperTechPage } from '../pages/reseach-paper-tech/reseach-paper-tech'
import { ReseachPaperComPage } from '../pages/reseach-paper-com/reseach-paper-com'
import { NotificationPage } from '../pages/notification/notification'
import { HttpModule } from '@angular/http'
import { MenuPage } from '../pages/menu/menu'
import { MenuBarPage } from '../pages/menubar/menubar'

import { EventPage } from '../pages/event/event';
import { DtsPage } from '../pages/dts/dts'
import { IonicStorageModule } from '@ionic/storage';

//firebase

import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFireModule } from 'angularfire2';



import { } from 'angularfire2/';
import { FirebaseProvider } from './../providers/firebase/firebase';
import { VideoPage } from '../pages/video/video';


//order
import { Ng2OrderModule } from 'ng2-order-pipe';
import { ThesisPage } from '../pages/thesis/thesis';

//new advisor
import { AdvisorPage } from '../pages/advisor/advisor'
import { ScholarshipMorePage } from '../pages/scholarship-more/scholarship-more'
import { ScholarshipViewPage } from '../pages/scholarship-view/scholarship-view'
import { SettingPage } from '../pages/setting/setting'
import { ResPage } from '../pages/res/res'
const firebaseConfig = {
  apiKey: "AIzaSyB52xVUNxD0VekZw5x6kFrEUeVQH2YLwGo",
  authDomain: "gradnu-zenith.firebaseapp.com",
  databaseURL: "https://gradnu-zenith.firebaseio.com",
  projectId: "gradnu-zenith",
  storageBucket: "gradnu-zenith.appspot.com",
  messagingSenderId: "498467065295"
};
import { Network } from '@ionic-native/network';

//chart
import { SocialSharing } from '@ionic-native/social-sharing';
import { ChartsModule } from 'ng2-charts/charts/charts';
import '../../node_modules/chart.js/dist/Chart.bundle.min.js';
import { OneSignal } from '@ionic-native/onesignal';
import { CalendarEventPage } from '../pages/calendar-event/calendar-event'

//chart
import { CalendarViewPage } from '../pages/calendar-view/calendar-view';
import { AppsetProvider } from '../providers/appset/appset';


import { QuestionnairePage } from '../pages/questionnaire/questionnaire';

import { AnnouncePage } from '../pages/announce/announce';
import { AnnounceNormalPage } from '../pages/announce-normal/announce-normal';
import { AnnouncelistPage } from '../pages/announcelist/announcelist';
import { AnnounceviewPage } from '../pages/announceview/announceview';


import { SurvayPage } from "../pages/survay/survay";
import { KeysPipe } from "../pipes/keys/keys";
import { CoreService } from "../services/core-service";

import { TestPage } from "../pages/test/test";
import { SurvayViewsPage } from "../pages/survay-views/survay-views";
import { MenuipadPage } from "../pages/menuipad/menuipad";
import { QrcodeIpadPage } from "../pages/qrcode-ipad/qrcode-ipad";
import { ContactPage } from "../pages/contact/contact";
import { LaunchNavigator, LaunchNavigatorOptions } from '@ionic-native/launch-navigator';
import { DtsViewPage } from "../pages/dts-view/dts-view";

import { CameraPreview } from '@ionic-native/camera-preview';
import { Diagnostic } from '@ionic-native/diagnostic';
import { GpaxPage } from '../pages/gpax/gpax';
import { AdvisorDisPage } from '../pages/advisor-dis/advisor-dis';
import { EngPage } from "../pages/eng/eng";
import { CalendarEventViewsPage } from "../pages/calendar-event-views/calendar-event-views";

import { GuestsPage } from "../pages/guests/guests";

import { GuestsCoursePage } from '../pages/guests-course/guests-course';
import { GuestsNotificationPage } from '../pages/guests-notification/guests-notification';
import { GuestsRegisterPage } from '../pages/guests-register/guests-register';
import { VideoViewPage } from '../pages/video-view/video-view'
import { NotificationBackgroundPage } from '../pages/notification-background/notification-background';
import { GuestsNotificationViewsPage } from '../pages/guests-notification-views/guests-notification-views';
import { WheelSelector } from '@ionic-native/wheel-selector';
import { Vibration } from '@ionic-native/vibration';
import { GuestsCourseViewsPage} from '../pages/guests-course-views/guests-course-views'
import {InterviewsPage} from '../pages/interviews/interviews';
import {ReportPage} from '../pages/report/report'

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    LoginPage,
    //modal
    ModalInfoPage,
    ModalcalendarPage,
    Modalnotification,
    Modalcapital,
    Modaltalk,
    Modalqrcode,
    //book
    BookService,
    BookBuy,
    BookNews,
    BookContact,
    //mange
    ManageNotifiction,
    ManageAlumni,
    //media
    MediaKnow,
    MediaNotifiction,
    //reseach
    ResearchCapital,
    ResearchPaper,
    AddEventPage,
    CapitalPage,
    BookServiceDetailPage,
    BookOtherbookDetailPage,
    ReseachPaperTechPage,
    ReseachPaperComPage,
    NotificationPage,
    VideoViewPage,
    MenuPage,
    MenuBarPage,
    AdvisorPage,
    ScholarshipMorePage,
    ScholarshipViewPage,
    SettingPage,
    CalendarViewPage,
    CalendarEventPage,
    AnnouncePage,
    QuestionnairePage,
    AnnounceNormalPage,
    AnnouncelistPage,
    VideoPage,
    AnnounceviewPage,
    SurvayPage,
    KeysPipe,
    TestPage,
    EventPage,
    DtsPage,
    ResPage,
    SurvayViewsPage,
    MenuipadPage,
    QrcodeIpadPage,
    ContactPage,
    DtsViewPage,
    CameraPage,
    GpaxPage,
    AdvisorDisPage,
    ThesisPage,
    EngPage,
    CalendarEventViewsPage,
    GuestsPage,
    GuestsCoursePage,
    GuestsNotificationPage,
    GuestsRegisterPage,
    NotificationBackgroundPage,
    GuestsNotificationViewsPage,
    GuestsCourseViewsPage,
    InterviewsPage,
    ReportPage
  ],
  imports: [
    CalendarModule,
    BrowserModule,
    IonicModule.forRoot(MyApp, {
      platforms: {
        ios: {
          swipeBackEnabled: false
        },
      }
    }),
    HttpModule,
    AngularFireDatabaseModule,                         //firebase
    AngularFireModule.initializeApp(firebaseConfig),   //firebase
    IonicStorageModule.forRoot(),
    ChartsModule,
    Ng2OrderModule, //chart


  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    LoginPage,
    ModalInfoPage,
    ModalcalendarPage,
    Modalnotification,
    Modalcapital,
    Modaltalk,
    Modalqrcode,
    //book
    BookService,
    BookBuy,
    BookNews,
    BookContact,
    //mange
    ManageNotifiction,
    ManageAlumni,
    //media
    MediaKnow,
    MediaNotifiction,
    //reseach
    ResearchCapital,
    ResearchPaper,
    AddEventPage,
    CapitalPage,
    BookServiceDetailPage,
    BookOtherbookDetailPage,
    ReseachPaperTechPage,
    ReseachPaperComPage,
    NotificationPage,
    MenuPage,
    MenuBarPage,
    AdvisorPage,
    ScholarshipMorePage,
    ScholarshipViewPage,
    SettingPage,
    CalendarViewPage,
    CalendarEventPage,
    AnnouncePage,
    QuestionnairePage,
    AnnounceNormalPage,
    AnnouncelistPage,
    AnnounceviewPage,
    SurvayPage,
    VideoViewPage,
    VideoPage,
    EventPage,
    DtsPage,
    ResPage,
    SurvayViewsPage,
    MenuipadPage,
    QrcodeIpadPage,
    ContactPage,
    DtsViewPage,
    CameraPage,
    GpaxPage,
    AdvisorDisPage,
    ThesisPage,
    EngPage,
    CalendarEventViewsPage,
    GuestsPage,
    GuestsCoursePage,
    GuestsNotificationPage,
    GuestsRegisterPage,
    NotificationBackgroundPage,
    GuestsNotificationViewsPage,
    GuestsCourseViewsPage,
    InterviewsPage,
    ReportPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    Calendar,
    {
      provide: ErrorHandler, useClass: IonicErrorHandler
    },
    FirebaseProvider,
    SocialSharing,
    OneSignal,
    AppsetProvider,
    Network,
    CoreService,
    LaunchNavigator,
    CameraPreview,
    Diagnostic,
    WheelSelector,
    Vibration,

  ]
})
export class AppModule { }
