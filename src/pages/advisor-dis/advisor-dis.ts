import { Component } from '@angular/core';
import { NavController, NavParams ,ViewController} from 'ionic-angular';

/**
 * Generated class for the AdvisorDisPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-advisor-dis',
  templateUrl: 'advisor-dis.html',
})
export class AdvisorDisPage {

  constructor(public navCtrl: NavController, public navParams: NavParams ,public viewCtrl:ViewController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AdvisorDisPage');
  }
  dismiss() {
    this.viewCtrl.dismiss();
  }
  
}
