import { Component } from '@angular/core';
import { NavController, NavParams ,ViewController} from 'ionic-angular';
import { Http, Headers, RequestOptions } from '@angular/http'
import { FirebaseProvider } from '../../providers/firebase/firebase';
/**
 * Generated class for the AdvisorPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-advisor',
  templateUrl: 'advisor.html',
})
export class AdvisorPage {
  items={
    'position':'',
    'Exp':'',
    'first_name':'',
    'last_name':'',
    'position_eng':'',
    'first_name_eng':'',
    'faculty_eng':'',
    'work':'',
    'faculty':'',
  };
  constructor(public navCtrl: NavController, public navParams: NavParams ,public viewCtrl:ViewController,
  
      public http: Http,
      public db:FirebaseProvider
    ) {


 
  }

  ionViewDidLoad() {
    this.db.showLoading()

      let headers = new Headers({
        'Content-Type': 'application/json',
        "Authorization": 'JWT ' + localStorage.getItem('Token')
      });
      let option = new RequestOptions({
        headers: headers
      });



      let urlApi = 'http://www.db.grad.nu.ac.th/apps/ws/getAdvisorAssignStudInfo/' + localStorage.getItem('UserId')
      this.http.post(urlApi, null, option).map(x => x.json()).subscribe(data => {
        let advisorUrl = 'http://www.db.grad.nu.ac.th/apps/ws/getGradStaff/' + data.result[0].advisor_id
        this.http.post(advisorUrl, null, option)
          .map(x => x.json())
          .subscribe(snapshot => {
            this.items=snapshot.result[0]
            let urlApi = 'http://www.db.grad.nu.ac.th/apps/ws/getNUEducation/'+snapshot.result[0].nu_staff

            this.http.post(urlApi, null, option).map(x => x.json()).subscribe(data => {
              console.log( )
              data.result.forEach(element => {
                if(element.BYTEDES=='ปริญญาเอก'&&element['สถานะศึกษา']=="สำเร็จการศึกษา")
                this.items['Exp']=" ดร. "
                this.items['DR']=" Dr. "
              });
            });
          });

          this.db.stopLoading()

  });
}

  dismiss() {
    this.viewCtrl.dismiss();
  }
  

}
 // if (bool == true) {
    //   this.showLoading()
    //   let headers = new Headers({
    //     'Content-Type': 'application/json',
    //     "Authorization": 'JWT ' + localStorage.getItem('Token')
    //   });
    //   let option = new RequestOptions({
    //     headers: headers
    //   });
    //   let urlApi = 'http://www.db.grad.nu.ac.th/apps/ws/getAdvisorAssignStudInfo/' + localStorage.getItem('UserId')
    //   this.http.post(urlApi, null, option).map(x => x.json()).subscribe(data => {
    //     let advisorUrl = 'http://www.db.grad.nu.ac.th/apps/ws/getGradStaff/' + data.result[0].advisor_id
    //     this.http.post(advisorUrl, null, option)
    //       .map(x => x.json())
    //       .subscribe(snapshot => {
    //         if (this.platform.is('ipad')) {
    //           let myModal = this.modalCtrl.create(AdvisorPage, {
    //             'status': snapshot.result[0],
    //             'nu_staff': snapshot.result[0].nu_staff
    //           });
    //           $(".body").addClass("active");
    //           myModal.onDidDismiss(data => {
    //             $(".body").removeClass("active");
    //           });
    //           myModal.present();
    //           this.loading.dismiss();
    //         } else {

    //           this.loading.dismiss();
    //           $(".body").addClass("active");
    //           let myModal = this.modalCtrl.create(AdvisorPage, {
    //             'status': snapshot.result[0],
    //             'nu_staff': snapshot.result[0].nu_staff
    //           });

    //           myModal.present();
    //           myModal.onDidDismiss(data => {
    //             $(".body").removeClass("active");
    //           });
    //         }
    //       })
    //   })
    // } else {
    //   // this.navCtrl.push(GpaxPage)
    //   let myModal = this.modalCtrl.create(AdvisorDisPage);
    //   myModal.present();
    //   $(".body").addClass("active");
    //   myModal.onDidDismiss(data => {
    //     $(".body").removeClass("active");
    //   });
    // }