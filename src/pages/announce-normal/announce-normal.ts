import { Component } from '@angular/core';
import { NavController, NavParams ,ViewController} from 'ionic-angular';

/**
 * Generated class for the AnnounceNormalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-announce-normal',
  templateUrl: 'announce-normal.html',
})
export class AnnounceNormalPage {
  item:any
  constructor(public navCtrl: NavController, public navParams: NavParams ,public viewCtrl:ViewController) {
    this.item=navParams.get('data')
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AnnounceNormalPage');
  }
  dismiss(){
      this.viewCtrl.dismiss()
  }
}
