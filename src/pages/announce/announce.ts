import { Component } from '@angular/core';
import { NavController, NavParams, ModalController, ViewController, AlertController } from 'ionic-angular';
import { FirebaseProvider } from './../../providers/firebase/firebase';
import { DEFAULT_INTERPOLATION_CONFIG } from '@angular/compiler';
/**
 * Generated class for the AnnouncePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-announce',
  templateUrl: 'announce.html',
})
export class AnnouncePage {
  item: any
  count = 0;
  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController, public db: FirebaseProvider, public alertCtrl: AlertController) {
    this.item = navParams.get('data')

  }

  ionViewDidLoad() {
    setTimeout(() => {
      this.viewCtrl.dismiss()
    }, 4000);
  }
  
  register(data) {
    let alert = this.alertCtrl.create({
      title: 'การลงทะเบียนกิจกรรม',
      message: 'ยืนยันการลงทะเบียนกิจกรรม',
      buttons: [
        {
          text: 'ยกเลิก',
          role: 'cancel',
          handler: () => {

          }
        },
        {
          text: 'ตกลง',
          handler: () => {
            this.db.register(data, data.Attendees.unit)
            this.viewCtrl.dismiss()
          }
        }
      ]
    });
    alert.present();
  }

  dismiss() {
    this.viewCtrl.dismiss()
  }
  checkregister(item) {

    let status
    let BreakException = {}
    try {
      Object.keys(item.Attendees.interes).forEach(function (el) {

        if (el == localStorage.getItem('UserId')) {
          status = 1
          throw BreakException;
        } else {
          status = 0
        }
      });
    } catch (e) {
      if (e !== BreakException) throw e;
    }
    return status
  }

}
