import { Component } from '@angular/core';
import { NavController, NavParams, ViewController, LoadingController, Loading, AlertController } from 'ionic-angular';
import { Calendar } from '@ionic-native/calendar';
import { SocialSharing } from '@ionic-native/social-sharing';
import { FirebaseProvider } from './../../providers/firebase/firebase';
@Component({
  selector: 'page-announceview',
  templateUrl: 'announceview.html',
})
export class AnnounceviewPage {
  loading: Loading;
  public ElementObject: any
  register_status=0;
  event = { title: "", location: "", message: "", startDate: "", endDate: "" };
  constructor(public navCtrl: NavController
    , public navParams: NavParams
    , public viewCtrl: ViewController
    , public socialSharing: SocialSharing
    , public loadingCtrl: LoadingController
    , public db: FirebaseProvider
    , public calendar: Calendar
    , public alertCtrl: AlertController
  ) {

    this.ElementObject = this.navParams.get('object');
  }

  ionViewDidLoad() {
    this.checkregister(this.ElementObject)
  }
  dismiss() {
    this.viewCtrl.dismiss()
  }
  ScholarshipShare(item) {

    this.socialSharing.share(item.des, item.title, null, item.imgUrl).then(x => {
      console.log(x)
    }).catch(err => {
    });
  }
  register(data) {
    let alert = this.alertCtrl.create({
      title: 'การลงทะเบียนกิจกรรม',
      message: 'ยืนยันการลงทะเบียนกิจกรรม',
      buttons: [
        {
          text: 'ยกเลิก',
          role: 'cancel',
          handler: () => {

          }
        },
        {
          text: 'ตกลง',
          handler: () => {

            this.db.register(data, data.Attendees.unit);
            this.db.showLoading();
            setTimeout(() => {
              this.db.stopLoading();
              // this.viewCtrl.dismiss();
              this.navCtrl.popAll()
            }, 1200);
          }
        }
      ]
    });
    alert.present();
  }

  presentConfirm() {

  }
  showLoading() {
    this.loading = this.loadingCtrl.create({
      spinner: 'hide',
      content: `
        <div class="cssload-container" name="bubbles">
            <div class="cssload-whirlpool"></div>
            <img src="assets/ui/load.png" alt="" width="42">
        </div>
        `,

      cssClass: 'loader',
      showBackdrop: true,
      dismissOnPageChange: false

    });
    this.loading.present();

  }
  checkregister(item) {
    let obj =item.Attendees.interes
   if(obj[localStorage.getItem('UserId')]){
    this.register_status=1
   }else{
    this.register_status=0
   }

   
  }

  decode(time) {
    var d = new Date(time);
    var date = d.getDate();
    var Month = d.getMonth();
    var year = d.getFullYear();

    var hr = d.getHours();
    var min = d.getMinutes();
    var sec = d.getSeconds()
    var thmonth = new Array("มกราคม", "กุมภาพันธ์", "มีนาคม",
      "เมษายน", "พฤษภาคม", "มิถุนายน", "กรกฎาคม", "สิงหาคม", "กันยายน",
      "ตุลาคม", "พฤศจิกายน", "ธันวาคม");
    return date + " " + thmonth[Number(Month)] + " " + Number(year + 543)
  }
  min(data) {
    if (data < 10) {
      return "0" + data;
    } else {
      return data
    }
  }

}
