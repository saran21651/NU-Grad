import { Component } from '@angular/core';
import { NavParams, ViewController }  from 'ionic-angular';

@Component({
  templateUrl: 'book-buy.html'
})
export class BookBuy {
  myParam: string;

  constructor(
    public viewCtrl: ViewController,
    params: NavParams
  ) {
    this.myParam = params.get('myParam');
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }
}