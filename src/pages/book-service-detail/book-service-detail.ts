import { Component } from '@angular/core';
import { NavController, NavParams ,ViewController} from 'ionic-angular';

/**
 * Generated class for the BookServiceDetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-book-service-detail',
  templateUrl: 'book-service-detail.html',
})
export class BookServiceDetailPage {

  constructor(public navCtrl: NavController, public navParams: NavParams ,public viewCtrl: ViewController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad BookServiceDetailPage');
  }
  dismiss() {
    this.viewCtrl.dismiss();
  }
}
