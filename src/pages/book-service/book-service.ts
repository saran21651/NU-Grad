import { Component } from '@angular/core';
import { NavParams, ViewController,ModalController }  from 'ionic-angular';
import {BookServiceDetailPage} from '../book-service-detail/book-service-detail'
import {BookOtherbookDetailPage} from '../book-otherbook-detail/book-otherbook-detail'

@Component({
  templateUrl: 'book-service.html'
})
export class BookService {



  myParam: string;
  constructor(
    public viewCtrl: ViewController,
    params: NavParams,
    public modalCtrl:ModalController
  ) {
    this.myParam = params.get('myParam');
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }
  makebook(){
    let myModal = this.modalCtrl.create(BookServiceDetailPage);
    myModal.present();
  }
  makeother(){
    let   modal = this.modalCtrl.create(BookOtherbookDetailPage);
    modal.present();
  }
}