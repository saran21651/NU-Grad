import { Component } from '@angular/core';
import { NavController, NavParams,ViewController } from 'ionic-angular';
import { FirebaseProvider } from './../../providers/firebase/firebase';

/**
 * Generated class for the CalendarEventViewsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-calendar-event-views',
  templateUrl: 'calendar-event-views.html',
})
export class CalendarEventViewsPage {
  items=this.navParams.get('items')
  constructor(public navCtrl: NavController, public navParams: NavParams,public viewCtrl:ViewController,public db:FirebaseProvider) {
    this.db.showLoading()
    this.db.stopLoading()
  }

  ionViewDidLoad() {
    console.log(this.items)
  }
  startdate(time){
    var d = new Date(time)
    return d.getDate()
  }
  enddate(time){
    var d = new Date(time)
    return d.getDate()
  }
  mounth(time){

    var d = new Date(time)
    var monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
    var thmonth = new Array ("มกราคม","กุมภาพันธ์","มีนาคม",
    "เมษายน","พฤษภาคม","มิถุนายน", "กรกฎาคม","สิงหาคม","กันยายน",
    "ตุลาคม","พฤศจิกายน","ธันวาคม");
    return thmonth[d.getMonth()]
  }
  year(time){
    var d = new Date(time)
    return d.getFullYear()+543
  }
  dismiss(){
    this.viewCtrl.dismiss()
  }
}
