import { Component } from '@angular/core';
import { NavController, NavParams, ViewController, AlertController,ModalController } from 'ionic-angular';
import { Calendar } from '@ionic-native/calendar';
import { CalendarEventViewsPage} from '../calendar-event-views/calendar-event-views'
import { FirebaseProvider } from './../../providers/firebase/firebase';
declare var jquery: any;
declare var $: any;
/**
 * Generated class for the CalendarEventPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-calendar-event',
  templateUrl: 'calendar-event.html',
})
export class CalendarEventPage {
  Element: any
  startDate: any;
  endDate: any
  items = []
  date:any=this.navParams.get('day');
  mm:any=this.navParams.get('mm');
  yy:any=this.navParams.get('yy');
  object: any
  constructor(public navCtrl: NavController, 
    public navParams: NavParams, public viewCtrl: ViewController, public calendar: Calendar, public alertCtrl: AlertController
    ,public modelCtrl:ModalController
    ,public db:FirebaseProvider
  ) {



  }

  ionViewDidLoad() {
    this.db.showLoading();
    this.Element = this.navParams.get('Element')
    this.db.stopLoading()

    console.log(this.Element)
    this.items = $.map(this.Element, function (value, index) {
      return [value];
    });



  }
  dismiss() {
    this.viewCtrl.dismiss();
  }


  save() {


  }
  setdate(data) {

    var x = data
    var str = x.slice(0, 10);
    var year = x.slice(0, 4) - 543;
    var mount = x.slice(4, 7);
    var date = x.slice(7, 10);

    return year + mount + date
  }
  
  startdate(time){
    var d = new Date(time)
    return d.getDate()
  }
  enddate(time){
    var d = new Date(time)
    return d.getDate()
  }
  mounth(time){

    var d = new Date(time)
    var monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
    var thmonth = new Array ("มกราคม","กุมภาพันธ์","มีนาคม",
    "เมษายน","พฤษภาคม","มิถุนายน", "กรกฎาคม","สิงหาคม","กันยายน",
    "ตุลาคม","พฤศจิกายน","ธันวาคม");
    return thmonth[d.getMonth()]
  }
  year(time){
    var d = new Date(time)
    return d.getFullYear()+543
  }
  popup(item){
    let modal=this.modelCtrl.create(CalendarEventViewsPage,{"items":item})
    modal.present()
  }
}
