import { Component } from '@angular/core';
import {
  NavController,
  NavParams,
  ViewController,
  Platform,
  ModalController,
  ToastController

} from 'ionic-angular';
import { Calendar } from '@ionic-native/calendar';
import { FirebaseProvider } from './../../providers/firebase/firebase';
import { CalendarEventViewsPage } from "../calendar-event-views/calendar-event-views"
declare var jquery: any;
declare var $: any;

/**
 * Generated class for the CalendarViewPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-calendar-view',
  templateUrl: 'calendar-view.html',
})
export class CalendarViewPage {
  items = []
  Element: any
  startDate: any;
  endDate: any
  dataset = []
  object: any
  loadNumber = 15;
  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController, public calendar: Calendar, public platform: Platform, public modalCtrl: ModalController, public toastCtrl: ToastController, public db: FirebaseProvider) {

  }

  ionViewDidLoad() {

  }
  dismiss() {
    this.viewCtrl.dismiss();
  }


  ionViewWillEnter() {

    this.loadCalendar()
  }
  loadCalendar() {
    this.db.showLoading()
    this.items = []
    this.dataset = []
    this.db.callevent().limitToFirst(this.loadNumber).once('value', snap => {
      var d = new Date(snap.key);
      var current = new Date();

      if (d.getMonth == current.getMonth) {

        var arr = snap.val()
        var data = $.map(arr, function (value, index) {
          return [value];
        });

        data.forEach((element, index) => {
          var data = $.map(element, function (value, index) {
            return [value];
          });

          this.items.push(data)



        });

        var d = new Date()

        for (let i = 0; i < this.items.length; i++) {

          this.items[i].forEach(element => {

            if (d.getTime() <= element.start) {
              this.dataset.push(element);
            }



          });
        }
      }

      this.db.stopLoading();
    })
  }
  startdate(time) {
    var d = new Date(time)
    return d.getDate()
  }
  enddate(time) {
    var d = new Date(time)
    return d.getDate()
  }
  mounth(time) {

    var d = new Date(time)
    var monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
    var thmonth = new Array("มกราคม", "กุมภาพันธ์", "มีนาคม",
      "เมษายน", "พฤษภาคม", "มิถุนายน", "กรกฎาคม", "สิงหาคม", "กันยายน",
      "ตุลาคม", "พฤศจิกายน", "ธันวาคม");
    return thmonth[d.getMonth()]
  }
  year(time) {
    var d = new Date(time)
    return d.getFullYear() + 543
  }
  popup(item) {
    let modal = this.modalCtrl.create(CalendarEventViewsPage, { "items": item })
    modal.present()
  }
  doRefresh(refresher) {
    this.loadNumber = this.loadNumber + 7

    setTimeout(() => {
      this.loadCalendar()
      refresher.complete();
    }, 2000);
  }

}
