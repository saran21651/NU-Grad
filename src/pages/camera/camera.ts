import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, Events, Platform, LoadingController,Loading} from 'ionic-angular';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { CameraPreview, CameraPreviewPictureOptions, CameraPreviewOptions, CameraPreviewDimensions } from '@ionic-native/camera-preview';

/**
 * Generated class for the CameraPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
import { Diagnostic } from '@ionic-native/diagnostic';
import { Slides } from 'ionic-angular';
import { FirebaseProvider } from './../../providers/firebase/firebase';
import {HomePage} from '../home/home';

import {InterviewsPage} from '../interviews/interviews'

import { Http, Headers, RequestOptions } from '@angular/http';
declare var jquery: any;
declare var $: any;

@Component({
  selector: 'page-camera',
  templateUrl: 'camera.html',
})
export class CameraPage {
  @ViewChild(Slides) slides: Slides;
  public base64Image: string;
  picture1: any;
  picture2: any;
  picture3: any;
  devicePlatform: any;
  loading: Loading;
  CameraPreview:any
  constructor(
              public cameraPreview: CameraPreview, 
              public navCtrl: NavController, 
              public navParams: NavParams, 
              private events: Events, 
              private plt: Platform,
              public Diagnostic: Diagnostic,
              public db:FirebaseProvider,
              public http: Http,
              public loadingCtrl: LoadingController,
            ) {


    if (this.plt.is('ios')) {
      this.devicePlatform = 'ios';
    }
    if (this.plt.is('android')) {
      this.devicePlatform = 'android';
    }

  }
  ionViewDidLoad() {
    this.slides.lockSwipes(true)
    if (this.devicePlatform == 'ios') {
      (window.document.querySelector('ion-app') as HTMLElement).classList.add('cameraView');

    }
    if (this.devicePlatform == 'android') {
      window.document.querySelector('ion-app').classList.add('transparentBody');
    }
  }
  ionViewDidLeave() {
    this.cameraPreview.stopCamera();

  }
  SetCamera() {

    var cameraPreviewOpts: CameraPreviewOptions = {
      x: 0,
      y: 0,
      width: window.screen.width,
      height: window.screen.height,

      tapPhoto: true,
      previewDrag: true,
      toBack: true,
      alpha: 1
    };
    this.cameraPreview.startCamera(cameraPreviewOpts).then(
      (res) => {
        console.log(res)

      },
      (err) => {


      });

    this.slides.lockSwipes(false)
    this.slides.slideTo(1, 500);
    this.slides.lockSwipes(true)
    this.CameraPreview=true
  }

  swip(data) {
    
    if (data == 1) {
      this.takepic(data)
      this.slides.lockSwipes(false)
      this.slides.slideTo(2, 500);
      this.slides.lockSwipes(true)

    } else if (data == 2) {
      this.takepic(data)
      this.slides.lockSwipes(false)
      this.slides.slideTo(3, 500);
      this.slides.lockSwipes(true)
    } else if (data == 3) {
      this.takepic(data)
  
      $(".home").addClass("camera2");


      this.hideBackground()
      this.slides.lockSwipes(false)
      this.slides.slideTo(4, 500);
      this.slides.lockSwipes(true)
      
    
    } else if (data == 4) {
      this.slides.lockSwipes(true)

      this.db.storage(this.picture1,this.picture2,this.picture3,localStorage.getItem('UserId'))
      this.get_student_info()
    
      
  
    }
  }
 
  swipeCamera() {
    console.log('dag')
    this.cameraPreview.switchCamera();
  }
  takepic(data) {
    const pictureOpts: CameraPreviewPictureOptions = {
      width: 1280,
      height: 1280,
      quality: 85
    }
    this.cameraPreview.takePicture(pictureOpts).then((imageData) => {
      if (data == 1) {
        this.picture1 = 'data:image/jpeg;base64,' + imageData;
      } else if (data == 2) {
        this.picture2 = 'data:image/jpeg;base64,' + imageData;
      } else if (data == 3) {
        this.picture3 = 'data:image/jpeg;base64,' + imageData;
      }
    }, (err) => {
      console.log(err);

    });
  }

  get_student_info() {
    this.db.showLoading();
    var data
    let urlApi = 'http://www.db.grad.nu.ac.th/apps/ws/get_student_info/' + localStorage.getItem('UserId')
    let headers = new Headers({ 'Content-Type': 'application/json', "Authorization": 'JWT ' + localStorage.getItem('Token') });
    let option = new RequestOptions({ headers: headers });
    this.http.post(urlApi, null, option)
        .map(x => x.json())
          .subscribe(data => {
            this.db.stopLoading();

            if(localStorage.getItem('playerId')){
              this.db.updatePlayerID(localStorage.getItem('playerId'))
            }
              this.db.checkin(data.result[0])
           
              localStorage.setItem('types',data.result[0].PROGRAMID)
              localStorage.setItem('GPA',data.result[0].GPA)
                                      //Next page
         
              this.loading.dismiss();
              this.navCtrl.push(HomePage, { 'studentInfo': data.result[0] })
    }, error => {
      console.log(error);
      this.loading.dismiss();
    });
  }


  hideBackground(){
    if (this.devicePlatform == 'ios') {
      (window.document.querySelector('ion-app') as HTMLElement).classList.remove('cameraView');
    }
    if (this.devicePlatform == 'android') {
      window.document.querySelector('ion-app').classList.remove('transparentBody');
    }
  }

}
