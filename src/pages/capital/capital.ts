import { Component } from '@angular/core';
import { NavController, NavParams ,ViewController} from 'ionic-angular';

/**
 * Generated class for the CapitalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-capital',
  templateUrl: 'capital.html',
})
export class CapitalPage {

  constructor(public navCtrl: NavController, public navParams: NavParams , public viewCtrl:ViewController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CapitalPage');
  }
  dismiss() {
    this.viewCtrl.dismiss();
  }
}
