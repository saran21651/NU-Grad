import { Component, ViewChild, ElementRef } from '@angular/core';
import { NavController, NavParams, ViewController, AlertController } from 'ionic-angular';
import { LaunchNavigator, LaunchNavigatorOptions } from '@ionic-native/launch-navigator';
import * as firebase from 'firebase';
declare var google;
import {FirebaseProvider} from './../../providers/firebase/firebase';
/**
 * Generated class for the ContactPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-contact',
  templateUrl: 'contact.html',
})
export class ContactPage {

  @ViewChild('map') mapElement: ElementRef;
  map: any;
  items = ""
  constructor(public navCtrl: NavController,
     public navParams: NavParams, public viewCtrl: ViewController, 
     public launchNavigator: LaunchNavigator,
      public alertCtrl: AlertController,
      public db:FirebaseProvider
    ) {
  }

  ionViewDidLoad() {

    this.loadMap()
  }
  loadMap() {
    this.db.showLoading();
    let latLng = new google.maps.LatLng(16.741948, 100.191757);

    let mapOptions = {
      center: latLng,
      zoom: 17,
      // mapTypeId: 'satellite'

    }

    this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
    var marker = new google.maps.Marker({
      position: latLng,
      map: this.map,

    });
    this.db.stopLoading();
  }
  dismiss() {
    this.viewCtrl.dismiss()
  }
  go() {
    let options: LaunchNavigatorOptions = {
      start: '',
    };
    this.launchNavigator.navigate('"' + 16.741948 + ',' + 100.191757 + '"', options)
      .then(
        success => console.log('Launched navigator'),
        error => console.log('Error launching navigator', error)
      );
  }
  contact(data) {
    if(this.items!=""){
      let alert = this.alertCtrl.create({
        title: 'NU Grad',
        message: 'ยืนยันการส่งข้อมูล',
        buttons: [
          {
            text: 'ยกเลิก',
            role: 'cancel',
            handler: () => {
              console.log('Cancel clicked');
            }
          },
          {
            text: 'ยืนยัน',
            handler: () => {
              firebase.database().ref('student_contact').push({
                'text': data,
                'time': firebase.database.ServerValue.TIMESTAMP,
                'student_id': localStorage.getItem('UserId')
              })
            }
          }
        ]
      });
      alert.present();
  
  
    }else{
      let alert = this.alertCtrl.create({
        title: 'NU Grad',
        subTitle: 'กรุณากรอกข้อมูลให้ครบถ้วน',
        buttons: ['ตกลง']
      });
      alert.present();
    }
    

  }



}
