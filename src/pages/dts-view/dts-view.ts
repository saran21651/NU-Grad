import { Component } from '@angular/core';
import { NavController, NavParams ,ViewController} from 'ionic-angular';

/**
 * Generated class for the DtsViewPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-dts-view',
  templateUrl: 'dts-view.html',
})
export class DtsViewPage {
  items:any;
  constructor(public navCtrl: NavController, public navParams: NavParams,public viewCtrl:ViewController) {
    this.items=this.navParams.get('ele')
  }

  ionViewDidLoad() {
 
  }
  
  dismiss(){
    this.viewCtrl.dismiss()
  }

  decode(time) {
    var d = new Date(time);
    var date = d.getDate();
    var Month = d.getMonth();
    var year = d.getFullYear();

    var hr = d.getHours();
    var min = d.getMinutes();
    var sec = d.getSeconds()
    var thmonth = new Array ("มกราคม","กุมภาพันธ์","มีนาคม",
    "เมษายน","พฤษภาคม","มิถุนายน", "กรกฎาคม","สิงหาคม","กันยายน",
    "ตุลาคม","พฤศจิกายน","ธันวาคม");
    return date + " " + thmonth[Number(Month)] + " " + Number(year+543) + "  เวลา " + hr + ":" + this.min(min)+" น."
  }
  min(data){
    if(data<10){
      return "0"+data;
    }else{
      return data
    }
  }
}
