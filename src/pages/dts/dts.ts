import { Component } from '@angular/core';
import { NavController, NavParams ,ViewController,ModalController} from 'ionic-angular';
import {FirebaseProvider} from './../../providers/firebase/firebase';
import {DtsViewPage} from '../dts-view/dts-view';
declare var jquery: any;
declare var $: any;
@Component({
  selector: 'page-dts',
  templateUrl: 'dts.html',
})
export class DtsPage {
  items:any=[];
  items2:any;
  Load_No=8;
  Infi_load=false;
  constructor(public navCtrl: NavController,
     public navParams: NavParams 
     ,public db: FirebaseProvider,
     public viewCtrl:ViewController,
    public modalCtrl:ModalController) {
    this.setting()
  }

  ionViewDidLoad() {
 
  }
  setting(){
    this.db.showLoading();
    this.db.dts(this.Load_No).subscribe(doc=>{
      this.items=doc.reverse()
      this.db.stopLoading();
      this.Sort()
    })

  }


    Sort() {

    this.items.sort(function (a, b) { return a.time - b.time });
    this.items.reverse()

  }
  
  gopage(data){
    // this.navCtrl.push(DtsViewPage,{'ele':data})
    let pages = this.modalCtrl.create(DtsViewPage,{'ele':data})
    pages.present()
  }
  dismiss(){
    this.viewCtrl.dismiss();
  }
 
  decode(time) {
    var d = new Date(time);
    var date = d.getDate();
    var Month = d.getMonth();
    var year = d.getFullYear();

    var hr = d.getHours();
    var min = d.getMinutes();
    var sec = d.getSeconds()
    var thmonth = new Array ("มกราคม","กุมภาพันธ์","มีนาคม",
    "เมษายน","พฤษภาคม","มิถุนายน", "กรกฎาคม","สิงหาคม","กันยายน",
    "ตุลาคม","พฤศจิกายน","ธันวาคม");
    return date + " " + thmonth[Number(Month)] + " " + Number(year+543);
  }
  min(data){
    if(data<10){
      return "0"+data;
    }else{
      return data
    }
  }
  doRefresh(refresher) {
    setTimeout(() => {
   this.load();
      refresher.complete();
    }, 2000);
  }
  load(){
    this.Load_No=this.Load_No+8;
    this.db.dts(this.Load_No).subscribe(doc=>{
      this.items2=doc.reverse()
      this.items2 = $.map(this.items2, function (value, index) {
        return [value];
      });
      this.Sort2();
      if(this.items2.length==this.items.length){
        this.Infi_load=true;
        console.log(this.Infi_load)
      }
      for (let index = 0; index < this.items2.length; index++) {
        const element = this.items2[index];
        if (index >= this.items.length) {
          this.items.push(element)
       
        }
      }
      this.db.stopLoading();
      this.Sort()
    })

  }
  Sort2() {

    this.items2.sort(function (a, b) { return a.time - b.time });
    this.items2.reverse()

  }
  ref(refresher){
    console.log('Begin async operation', refresher);

    setTimeout(() => {
      this.db.dts(8).subscribe(doc=>{
        this.items=doc.reverse()
        
        this.Sort()
      })
  
      refresher.complete();
    }, 2000);
  }
}
