import { Component } from '@angular/core';
import { NavController, NavParams ,ViewController} from 'ionic-angular';
import {FirebaseProvider} from './../../providers/firebase/firebase';
/**
 * Generated class for the EngPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-eng',
  templateUrl: 'eng.html',
})
export class EngPage {
  element:any
  constructor(public navCtrl: NavController
            , public navParams: NavParams
            , public viewCtrl: ViewController
            , public db :FirebaseProvider
          ) {
            this.db.showLoading()
              this.element=this.navParams.get('data')
              this.db.stopLoading();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad EngPage');
  }
  dismiss() {
    this.viewCtrl.dismiss();
  }
  
}
