import { Component } from '@angular/core';
import { NavController, NavParams, ViewController, ModalController } from 'ionic-angular';
import { FirebaseProvider } from './../../providers/firebase/firebase';
import { AnnounceviewPage } from '../announceview/announceview';
import * as firebase from 'firebase';
/**
 * Generated class for the EventPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
declare var jquery: any;
declare var $: any;
@Component({
  selector: 'page-event',
  templateUrl: 'event.html',
})
export class EventPage {
  slides = [];
  items2:any;
  status = 0;
  loadNumber = 4;
  Infi_load=false
  constructor(public navCtrl: NavController, public navParams: NavParams, public db: FirebaseProvider, public viewCtrl: ViewController, public modalCtrl: ModalController) {
    this.setdata()
  }

  ionViewDidLoad() {
    
  }

  setdata() {
    this.db.showLoading();

    firebase.database().ref('popup')
        .orderByChild('type')
        .equalTo('event')
        .limitToLast(this.loadNumber)
        .once('value', snapshot => {

      let list = snapshot.val()

 
      var array = $.map(list, function (value, index) {
        return [value];
      });

      this.slides = array;
      this.Sort() 
      this.db.stopLoading()
    })

  }
  load(){
    firebase.database().ref('popup')
    .orderByChild('type').equalTo('event')
    .limitToLast(this.loadNumber)
    .once('value', snapshot => {
    
      this.items2 = $.map(snapshot.val(), function (value, index) {
        return [value];
      });
      if(this.items2.length==this.slides.length){
        this.Infi_load=true;
        console.log(this.Infi_load)
      }
      this.sort2()
      for (let index = 0; index < this.items2.length; index++) {
        const element = this.items2[index];
        if (index >= this.slides.length) {
          this.slides.push(element)
        }
      }
    })
  }
  sort2() {
    this.items2.sort(function (a, b) { return a.time - b.time });
    this.items2.reverse()
  }
  Sort() {

    this.slides.sort(function (a, b) { return a.time - b.time });
    this.slides.reverse()

  }
  dismiss() {
    this.viewCtrl.dismiss()
  }

  decode(time) {

    var d = new Date(time);
    var date = d.getDate();
    var Month = d.getMonth();
    var year = d.getFullYear();

    var hr = d.getHours();
    var min = d.getMinutes();
    var sec = d.getSeconds()

    var thmonth = new Array("มกราคม", "กุมภาพันธ์", "มีนาคม",
      "เมษายน", "พฤษภาคม", "มิถุนายน", "กรกฎาคม", "สิงหาคม", "กันยายน",
      "ตุลาคม", "พฤศจิกายน", "ธันวาคม");

    return date + " " + thmonth[Number(Month)] + " " + Number(year + 543)

  }
  showevent(data) {

    let myModal = this.modalCtrl.create(AnnounceviewPage, { 'object': data });
    myModal.present();
  }
  doRefresh(refresher) {
    this.loadNumber = this.loadNumber + 4

    setTimeout(() => {
      this.load()
      refresher.complete();
    }, 2000);
  }
  loadAgain(refresher) {
    console.log('Begin async operation', refresher);
    this.Infi_load=false
    setTimeout(() => {
      console.log('Async operation has ended');
      this.setdata()
      refresher.complete();
    }, 2000);
  }
}
