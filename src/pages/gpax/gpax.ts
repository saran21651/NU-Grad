import { Component } from '@angular/core';
import { NavController, NavParams ,ViewController} from 'ionic-angular';
import { Http, Headers, RequestOptions } from '@angular/http'
import {FirebaseProvider} from './../../providers/firebase/firebase';

/**
 * Generated class for the GpaxPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-gpax',
  templateUrl: 'gpax.html',
})
export class GpaxPage {
  grad: any;
  year:any=[];
  subject:any;
  team=[1,2,3];
  SEMESTER1=[];
  SEMESTER2=[];
  SEMESTER3=[];
  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public http: Http,
    public viewCtrl:ViewController,
    public db:FirebaseProvider
  ) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad GpaxPage');
    this.timetable()

  }


  timetable() {

    this.db.showLoading()
    let headers = new Headers({
      'Content-Type': 'application/json',
      "Authorization": 'JWT ' + localStorage.getItem('Token')
    });
    var sumATTEMPT = []
    let option = new RequestOptions({
      headers: headers
    });
    let urlApi = 'http://www.db.grad.nu.ac.th/apps/ws/get_transcript/' + localStorage.getItem('UserId')

    this.http.post(urlApi, null, option).map(x => x.json()).subscribe(data => {
      for (let index = 0; index < data.result.length; index++) {
        if(index==0){
          this.year.push(data.result[index].ACADYEAR)
        }else{
          this.setACADYEAR(data.result[index].ACADYEAR,data.result[index])

        }
        if(index==data.result.length-1){
          this.subject=data.result
          this.setteam(this.subject)
        }
      
      }
      this.db.stopLoading()
    });
  }

  setACADYEAR(a,b){
    if(this.year[this.year.length-1]!=a){
      this.year.push(a)
    }else{

    }
 
  }
  dismiss(){
    this.viewCtrl.dismiss()
  }
  sub(data){
    let yaer = data.toString()
    let StrYear = yaer.substring(2, 4);
    return StrYear
  }
  setteam(item){
    item.forEach(element => {
        if(element.SEMESTER==1){
          this.SEMESTER1.push(element)
        }
        else  if(element.SEMESTER==2){
          this.SEMESTER2.push(element)
        }
        else if(element.SEMESTER==3){
          this.SEMESTER3.push(element)     
          console.log(this.SEMESTER3.length)
        }
     
    });
  }
}
