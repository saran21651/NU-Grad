import { Component } from '@angular/core';
import { NavController, NavParams ,ViewController,ModalController} from 'ionic-angular';
import {GuestsCoursePage} from '../guests-course/guests-course'
import {FirebaseProvider} from './../../providers/firebase/firebase';
/**
 * Generated class for the GuestsCourseViewsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-guests-course-views',
  templateUrl: 'guests-course-views.html',
})
export class GuestsCourseViewsPage {

  constructor(public navCtrl: NavController,
           public navParams: NavParams,
           public viewCtrl:ViewController,
           public modalCtrl:ModalController,
           public db:FirebaseProvider
          ) {
            this.db.showLoading();
            this.db.stopLoading();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad GuestsCourseViewsPage');
  }
  gotopage(index,text){
    console.log(index)
    
    this.modalCtrl.create(GuestsCoursePage,{'ele':index,
                                            'curricula':text
  }).present()

  }
  dismiss(){
    
    this.viewCtrl.dismiss()
  }
}
