import { Component } from '@angular/core';
import { NavController, NavParams,ViewController,LoadingController,Loading} from 'ionic-angular';
import { Calendar } from '@ionic-native/calendar';
import { SocialSharing } from '@ionic-native/social-sharing';
/**
 * Generated class for the ScholarshipViewPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
import {FirebaseProvider} from './../../providers/firebase/firebase';
 
@Component({
  selector: 'page-guests-notification-views',
  templateUrl: 'guests-notification-views.html',
})
export class GuestsNotificationViewsPage {
  loading:Loading;
  public ElementObject:any

  event = { title: "", location: "", message: "", startDate: "", endDate: "" };
  constructor(public navCtrl: NavController
    , public navParams: NavParams
    ,public viewCtrl:ViewController
    ,public socialSharing:SocialSharing
    ,public loadingCtrl: LoadingController
    ,public db:FirebaseProvider
 
   ) {
    this.ElementObject=this.navParams.get('object');
   console.log(this.navParams.get('object')) 
  
  }

  ionViewDidLoad() {
 
  }
  dismiss() {
    this.viewCtrl.dismiss();
  }
  ScholarshipShare(item){
    
    this.socialSharing.share(item.des,item.title,null,item.imgUrl).then(x=>{
      console.log(x)
    }).catch(err=>{

    });

  
}
  register(data){
    this.db.register(data,data.Attendees.unit)
    this.navCtrl.popAll()
  }
  showLoading() {
    this.loading = this.loadingCtrl.create({
        spinner: 'hide',
        content: `
        <div class="cssload-container" name="bubbles">
            <div class="cssload-whirlpool"></div>
            <img src="assets/ui/load.png" alt="" width="42">
        </div>
        `,
     
        cssClass: 'loader',
        showBackdrop: true ,
        dismissOnPageChange:false

    });
    this.loading.present();

  }
  checkregister(item){

    let status
    let BreakException = {}
    try {
      Object.keys(item.Attendees.interes).forEach(function (el) {
        if (el == localStorage.getItem('UserId')) {
          status=1
          throw BreakException;
        }else{
          status=0
        }
      });
    } catch (e) {
      if (e !== BreakException) throw e;
    }
    return status
    }
  
    decode(time) {
      var d = new Date(time);
      var date = d.getDate();
      var Month = d.getMonth();
      var year = d.getFullYear();
  
      var hr = d.getHours();
      var min = d.getMinutes();
      var sec = d.getSeconds()
      var thmonth = new Array ("มกราคม","กุมภาพันธ์","มีนาคม",
      "เมษายน","พฤษภาคม","มิถุนายน", "กรกฎาคม","สิงหาคม","กันยายน",
      "ตุลาคม","พฤศจิกายน","ธันวาคม");
      return date + " " + thmonth[Number(Month)] + " " + Number(year+543)
    }
    min(data){
      if(data<10){
        return "0"+data;
      }else{
        return data
      }
    }

}
