import { Component } from '@angular/core';
import { NavController, NavParams, ViewController, ModalController } from 'ionic-angular';
import { FirebaseProvider } from './../../providers/firebase/firebase';
import { NotificationPage } from '../notification/notification';
import { GuestsNotificationViewsPage } from '../guests-notification-views/guests-notification-views';
import { AnnounceviewPage } from '../announceview/announceview'
declare var jquery: any;
declare var $: any;
/**
 * Generated class for the GuestsNotificationPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-guests-notification',
  templateUrl: 'guests-notification.html',
})
export class GuestsNotificationPage {
  items: any;
  slides: any;
  loadNumber = 6;
  items2: any;
  status = this.navParams.get('type');
  Infi_load = false
  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public viewCtrl: ViewController,
    public db: FirebaseProvider,
    public model: ModalController
  ) {

  }

  ionViewDidLoad() {
    this.db.showLoading()
    this.db.guests_notification2(this.loadNumber).subscribe(snap => {
      this.items = snap;
     
      this.Sort2()
      this.db.stopLoading();
    })
    this.Slides()
  }
  dismiss() {
    this.viewCtrl.dismiss()
  }

  decode(time) {
    var d = new Date(time);
    var date = d.getDate();
    var Month = d.getMonth();
    var year = d.getFullYear();

    var hr = d.getHours();
    var min = d.getMinutes();
    var sec = d.getSeconds()
    var thmonth = new Array("มกราคม", "กุมภาพันธ์", "มีนาคม",
      "เมษายน", "พฤษภาคม", "มิถุนายน", "กรกฎาคม", "สิงหาคม", "กันยายน",
      "ตุลาคม", "พฤศจิกายน", "ธันวาคม");
    return date + " " + thmonth[Number(Month)] + " " + Number(year + 543)
  }
  min(data) {
    if (data < 10) {
      return "0" + data;
    } else {
      return data
    }
  }
  NotificationPage(item) {

    if (this.status == true) {
      let modal = this.model.create(AnnounceviewPage, {
        'object': item
      })
      modal.onDidDismiss(data => {
        this.ionViewDidLoad()
      });

      modal.present()
    } else {
      let modal = this.model.create(GuestsNotificationViewsPage, {
        'object': item
      })
      modal.onDidDismiss(data => {
        this.ionViewDidLoad()
      });

      modal.present()
    }
  }
  news(item) {
    this.status
    if (this.status == true) {
      let modal = this.model.create(AnnounceviewPage, {
        'object': item
      })
      modal.onDidDismiss(data => {
        this.ionViewDidLoad()
      });
      modal.present()
    } else {
      let modal = this.model.create(GuestsNotificationViewsPage, {
        'object': item
      })
      modal.onDidDismiss(data => {
        this.ionViewDidLoad()
      });
      modal.present()
    }



  }
  Slides() {
    this.db.guests_notification().subscribe(snap => {
      this.slides = []
      this.slides = snap
      this.Sort()
    })

  }
  Sort() {

    this.slides.sort(function (a, b) { return a.time - b.time });
    this.slides.reverse()

  }
  Sort2() {

    this.items.sort(function (a, b) { return a.time - b.time });
    this.items.reverse()

  }
  load() {
    this.db.guests_notification2(this.loadNumber).subscribe(snap => {
      this.items2 = $.map(snap, function (value, index) {
        return [value];
      });
      this.sort3()
      console.log(this.items2)
      if(this.items2.length==this.items.length){
        this.Infi_load=true;
        console.log(this.Infi_load)
      }
      for (let index = 0; index < this.items2.length; index++) {
        const element = this.items2[index];
        if (index >= this.items.length) {
          this.items.push(element)
       
        }
      }
    })
  }
  sort3() {
    this.items2.sort(function (a, b) { return a.time - b.time });
    this.items2.reverse()
  }
  doRefresh(refresher) {
    this.loadNumber = this.loadNumber + 6
    setTimeout(() => {
      this.load()
      refresher.complete();
    }, 2000);
  }
  ref(refresher){
    console.log('Begin async operation', refresher);
    this.Infi_load=false
    setTimeout(() => {
      this.db.showLoading()
      this.db.guests_notification2(6).subscribe(snap => {
        this.items = snap;
        this.Sort2()
        this.db.stopLoading();
       
      })
      refresher.complete();
    }, 2000);
  }
}
