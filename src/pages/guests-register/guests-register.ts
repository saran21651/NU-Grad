import { Component } from '@angular/core';
import { NavController, NavParams ,ViewController,AlertController} from 'ionic-angular';
import {FirebaseProvider} from './../../providers/firebase/firebase';
/**
 * Generated class for the GuestsRegisterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-guests-register',
  templateUrl: 'guests-register.html',
})
export class GuestsRegisterPage {
  items:any={
      "name":"",
      "surname":"",
      "email":"",
      "tel":"",
      "coust":"",
      "interest":"",
      "oldEducational":"",
      "oldUniversity":"",
      "gpax":""
  };
  constructor(public navCtrl: NavController, 
              public navParams: NavParams ,
              public viewCtrl :ViewController,
              public db:FirebaseProvider,
              public alertCtrl:AlertController
            ) {
  }
  dismiss(){
    this.viewCtrl.dismiss()
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad GuestsRegisterPage');
  }
  save(obj){
   this.db.showLoading();
    if(
      obj.name==""
      ||obj.surname==""
      ||obj.email==""
      ||obj.tel==""
      ||obj.coust==""
      ||obj.interest==""
      ||obj.oldEducational==""
      ||obj.oldUniversity==""
      ||obj.gpax==""
     ){
       this.db.stopLoading();
      this.db.presentAlert('ลงทะเบียน','กรุณากรอกข้อมูลให้ครบถ้วน')
     }else{
  
      

      let alert2 = this.alertCtrl.create({
        title: 'ยืนยันการส่งข้อมูล',
        message: 'กดตกลงเพื่อยืนยัน',
        buttons: [
          {
            text: 'ยกเลิก',
            role: 'cancel',
            handler: () => {
              this.db.stopLoading();
            }
          },
          {
            text: 'ตกลง',
            handler: () => {
              this.db.stopLoading();

              alert('ลบทะเบียนเรียบร้อย')
              this.db.interesting(obj)
           
              this.navCtrl.pop()
            }
          }
        ]
      });
      alert2.present();
     }

  }
} 
