import { Component } from '@angular/core';
import { NavController, NavParams,ViewController,ModalController } from 'ionic-angular';

import {GuestsCoursePage} from '../guests-course/guests-course';
import {GuestsNotificationPage} from '../guests-notification/guests-notification';
import {GuestsRegisterPage} from '../guests-register/guests-register';
import {VideoPage} from '../video/video';
import {GuestsCourseViewsPage} from '../guests-course-views/guests-course-views'
/**
 * Generated class for the GuestsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-guests',
  templateUrl: 'guests.html',
})
export class GuestsPage {

  constructor(public navCtrl: NavController, public navParams: NavParams,public viewCtrl:ViewController,public modelCtrl:ModalController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad GuestsPage');

  }
  dismiss(){
    this.viewCtrl.dismiss()
  }
  gotopage(index){
      if(index==1){
           
        let modal = this.modelCtrl.create(GuestsNotificationPage);
        modal.present();
      }
      else if(index==2){
 
        let modal = this.modelCtrl.create(GuestsRegisterPage);
          modal.present();
      }
      else if(index==3){
        let modal = this.modelCtrl.create(GuestsCourseViewsPage);
        modal.present();
      }else if(index==4){
            
        let modal = this.modelCtrl.create(VideoPage);
          modal.present();
      }
  }
}
