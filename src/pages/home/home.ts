import {
  Component,
} from '@angular/core';
import { NavController, AlertController, ViewController, NavParams, ModalController, LoadingController, Loading, Platform, MenuController, } from 'ionic-angular';

// PAGE
import { ModalInfoPage } from '../modal-info/modal-info';
import { ModalcalendarPage } from '../modal-calendar/modal-calendar';
import { Modalnotification } from '../modal-notification/modal-notification';

import { Modalqrcode } from '../modal-qrcode/modal-qrcode';

import { MenuipadPage } from '../menuipad/menuipad'

//mange
import { AdvisorPage } from '../advisor/advisor'

import { AnnouncePage } from '../announce/announce'

import { Http, Headers, RequestOptions } from '@angular/http'
import 'rxjs/add/operator/map'
//
import { Storage } from '@ionic/storage';
//
import { MenuPage } from '../menu/menu';
import * as firebase from 'firebase';
import { ScholarshipMorePage } from '../scholarship-more/scholarship-more';
import { ScholarshipViewPage } from '../scholarship-view/scholarship-view';

import { EngPage } from '../eng/eng'

import { OneSignal } from '@ionic-native/onesignal';

import { FirebaseProvider } from './../../providers/firebase/firebase';

import { AnnounceNormalPage } from '../announce-normal/announce-normal'

import { GpaxPage } from '../gpax/gpax';
import { AdvisorDisPage } from '../advisor-dis/advisor-dis'
declare var jquery: any;
declare var $: any;
import { ThesisPage } from '../thesis/thesis';
import { AnnounceviewPage } from '../announceview/announceview';
import { NotificationBackgroundPage } from "../notification-background/notification-background";
import { GuestsNotificationPage } from "../guests-notification/guests-notification";
import { CalendarEventViewsPage } from "../calendar-event-views/calendar-event-views";
import { Network } from '@ionic-native/network';
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'

})

export class HomePage {
  public press: number = 0;
  public calendarswitch: any;
  public year: number = 543;
  grad: any
  user: any
  myParam: string;
  loading: Loading;
  public item: any;
  pet: string = "puppies";
  isAndroid: boolean = false;
  scholarshipItem: any
  slides = []
  noti: any;
  userid = {
    'Name': '',
    'Code': '',
    'ProgramName': '',
    'gpx': ''
  }
  viewStatus = 0;
  main_load = false;
  viewStatus2 = 0;
  percent: any = 0;
  engStatus = 0;
  advisorStatus = 0;
  researchHuman: any;
  count1: any = 0;
  count2: any = 0;
  result: any = 0;
  qualification = 0;
  PermitInfo = 0;
  publication = 0;
  thesisexam = 0;
  getCompleteInfo_value = 0;
  InfoEnd: any;
  onlineCalendar: any;
  event_calendar: any;
  items: any;
  faculty: any;
  setloads = 0;
  timeInterval: any;
  timeInterval_main: any;
  iload: any
  constructor(
    public navCtrl: NavController,
    public viewCtrl: ViewController,
    public params: NavParams,
    public modalCtrl: ModalController,
    public loadingCtrl: LoadingController,
    public http: Http,
    public storage: Storage,
    public platform: Platform,
    public oneSignal: OneSignal,
    public firebaseProvider: FirebaseProvider,
    public alertCtrl: AlertController,
    public menuCtrl: MenuController,
    public network: Network

  ) {
    this.toggle();
    this.setstatus()
    this.oneSignal.getIds().then((id) => {
      this.firebaseProvider.updatenotification(id)
    });
    this.get_student_info()


    this.popup();
    this.alertNotification();
    if (localStorage.getItem('playerId')) {
      this.one_login()
    }



  }
  Slides() {
    this.firebaseProvider.announcelist_slide().subscribe(snap => {
      this.slides = []
      this.slides = snap
      this.Sort()
    })
  }
  event_cal() {
    this.showLoading();
    this.items = [];
    this.event_calendar = []
    var index = 0;
    this.firebaseProvider.callevent().once('value', snap => {
      var d = new Date(snap.key);
      var current = new Date();

      if (d.getMonth == current.getMonth) {
        var arr = snap.val()
        var data = $.map(arr, function (value, index) {
          return [value];
        });
        data.forEach((element, index) => {
          var data = $.map(element, function (value, index) {
            return [value];
          });
          this.items.push(data)
        });

        var d = new Date()
        for (let i = 0; i < this.items.length; i++) {
          this.items[i].forEach(element => {

            if (d.getTime() <= element.start) {
              index++;
              if (index <= 3) {
                this.event_calendar.push(element);

              }

            }
          });

        }
      }
      this.loading.dismiss()
    })
  }
  Sort() {

    this.slides.sort(function (a, b) { return a.time - b.time });
    this.slides.reverse()

  }
  announce_more() {

    let myModal = this.modalCtrl.create(GuestsNotificationPage, { 'type': true });
    myModal.present();

  }
  popup() {


    let myModal
    this.firebaseProvider.popup().subscribe(snap => {
      snap.forEach(element => {
        if (this.viewStatus == 0) {
          this.viewStatus = 1

          myModal = this.modalCtrl.create(AnnouncePage, { 'data': element })
          myModal.present();

        }
      });
    })


  }
  get_student_info() {
    let urlApi = 'http://www.db.grad.nu.ac.th/apps/ws/get_student_info/' + localStorage.getItem('UserId')
    let headers = new Headers({ 'Content-Type': 'application/json', "Authorization": 'JWT ' + localStorage.getItem('Token') });
    let option = new RequestOptions({ headers: headers });

    this.http.post(urlApi, null, option).map(x => x.json()).subscribe(data => {
      this.userid.Name = data.result[0].STUDENTNAME + " " + data.result[0].STUDENTSURNAME
      this.userid.Code = data.result[0].STUDENTCODE
      this.userid.gpx = data.result[0].GPA;

    }, error => {
      console.log(error);

      this.loading.dismiss();
    });
  }

  //menu
  menu() {
    if (this.platform.is('ipad')) {
      let myModal = this.modalCtrl.create(MenuipadPage)
      myModal.present();
    } else {
      let myModal = this.modalCtrl.create(MenuPage)
      myModal.present();

    }
  }
  info() {

    this.navCtrl.push(ModalInfoPage);

  }
  notification() {

    if (this.platform.is('ipad')) {
      let myModal = this.modalCtrl.create(NotificationBackgroundPage);
      myModal.present();
    } else {


      let myModal = this.modalCtrl.create(Modalnotification);
      myModal.present();

    }

  }

  advisor(bool) {
    this.showLoading()
    if (bool == true) {
      let myModal = this.modalCtrl.create(AdvisorPage);
      myModal.present();
      this.loading.dismiss();
    } else {
      let myModal = this.modalCtrl.create(AdvisorDisPage);
      myModal.present();
      this.loading.dismiss();
    }
  }
  qrcode() {
    this.showLoading()

    if (this.platform.is('ipad')) {

      this.navCtrl.push(Modalqrcode, { 'type': this.userid.ProgramName })

      this.loading.dismiss();
    } else {
      this.loading.dismiss();
      let myModal = this.modalCtrl.create(Modalqrcode, { 'type': this.userid.ProgramName });

      myModal.present();
    }
  }

  //  ฟังก์ชั่นกดแล้วหมุนๆ
  menu_home() {
    $(".ring").addClass('fadein')
    $(".ring").removeClass("fadeout");

    $(".scholarship").removeClass('active')
    $(".calendar").removeClass('active')
    $(".announce").addClass('fadeout')
    $(".announce").removeClass('active')
  }

  menu_calendar() {


    $(".ring").addClass('fadeout')
    $(".ring").removeClass("fadein");
    $(".scholarship").addClass('fadeout')
    $(".scholarship").removeClass('active')
    $(".announce").addClass('fadeout')
    $(".announce").removeClass('active')


    $(".calendar").addClass('active')
    // this.calendar="calendar";
  }
  menu_scholarship() {


    $(".scholarship").addClass('active')

    $(".ring").addClass('fadeout')
    $(".ring").removeClass("fadein");
    $(".calendar").removeClass('active')
  }
  menu_announce() {

    $(".ring").addClass('fadeout')
    $(".ring").removeClass("fadein");

    $(".announce").addClass('active')


    $(".calendar").addClass('fadeout')
    $(".calendar").removeClass('active')
  }
  //  ฟังก์ชั่นกดแล้วหมุนๆ


  scholarship(object) {
    let myModal = this.modalCtrl.create(ScholarshipViewPage, { 'object': object });
    $(".body").removeClass("active");
    myModal.onDidDismiss(data => {

      $(".body").removeClass("active");
    });
    myModal.present();

  }
  scholarship_more() {
    let myModal = this.modalCtrl.create(ScholarshipMorePage);
    myModal.present();

  }

  ionViewDidLoad() {
    // this.homescolarship()  โหลด scolarship
    // this.Slides();          โหลด slide
    this.toggle();

    //this.event_cal()      // ดึงข้อมูลปฎิฑิน

    this.oneSignal.getIds().then((id) => {
      this.firebaseProvider.updatenotification(id)
    });
    if (localStorage.getItem('fist') == null || localStorage.getItem('fist') == undefined || !localStorage.getItem('fist')) {
      this.oneSignal.sendTag('แจ้งเตือนทั่วไป', 'true')
      this.oneSignal.sendTag('แจ้งเตือนกิจกรรม', 'true')
      this.oneSignal.sendTag('แจ้งเตือนทุนการศึกษา', 'true')
      localStorage.setItem('setting0', 'true');
      localStorage.setItem('setting1', 'true');
      localStorage.setItem('setting2', 'true');
      localStorage.setItem('fist', 'true')
    } else {


    }

  }

  toggle() {
    function carouselCreated(e, data) {
      var progress = {
        progress: 0
      };
      $(".node-contanier").addClass("active");
      setTimeout(function () {

        $(progress).animate({
          progress: 100
        }, {
            easing: 'easeOutBack',
            duration: 700,
            step: function (progress) {

              if (isNaN(progress))
                return; //for some easings we can get NaNs

              progress = progress / 100;
              var a = 320 + progress * 680;
              var b = 450 + progress * 230;
              var shiftY = -140 + progress * 140;
              var shiftZ = -2000 + progress * 580;

              $('.theta-carousel').theta_carousel({
                'path.settings.shiftZ': shiftZ,
                'path.settings.shiftY': shiftY,
                'path.settings.a': a,
                'path.settings.b': b
              });
            }
          });

      }, 300);
    }

    // $(document).ready(function() {

    var container = $("#menu");

    // fade in effect
    container.css({
      opacity: 0
    });
    container.delay(500).animate({
      opacity: 1
    }, 500);

    container.theta_carousel({
      selectedIndex: 0,
      distance: 40,
      numberOfElementsToDisplayRight: 1,
      numberOfElementsToDisplayLeft: 1,
      designedForWidth: 1900,
      designedForHeight: 2262,
      distanceInFallbackMode: 221,
      scaleX: 0.5,
      scaleY: 0.6,
      scaleZ: 1.36,
      path: {
        settings: {
          shiftY: -3123,
          shiftZ: -4192,

          rotationAngleZY: -101,
          a: 384,
          b: 945,
          endless: true
        },
        type: 'ellipse'
      },
      perspective: 893,
      sensitivity: 0.2,
      inertiaFriction: 9,
      fadeAway: true,
      fadeAwayNumberOfConfigurableElements: 27,
      fadeAwayBezierPoints: {
        p1: {
          x: 0,
          y: 99
        },
        p2: {
          x: 73,
          y: 41
        },
        p3: {
          x: 50,
          y: 26
        },
        p4: {
          x: 100,
          y: 18
        }
      },
      sizeAdjustment: true,
      sizeAdjustmentNumberOfConfigurableElements: 4,
      sizeAdjustmentBezierPoints: {
        p1: {
          x: 0,
          y: 100
        },
        p2: {
          x: 1,
          y: 61
        },
        p3: {
          x: 4,
          y: 30
        },
        p4: {
          x: 100,
          y: 49
        }
      },
      popoutSelected: true,
      popoutSelectedShiftY: -100,
      popoutSelectedShiftZ: -81
    });

    carouselCreated.call(container, null, {
      index: container.theta_carousel("option", "selectedIndex")

    });

    // });

  }
  ionViewWillEnter() {

    this.menuCtrl.swipeEnable(false)

    this.oneSignal.getIds().then((id) => {
      this.firebaseProvider.updatenotification(id)
    });
  }

  homescolarship() {
    this.showLoading()
    this.firebaseProvider.homescolarship()
      .subscribe(x => {
        this.scholarshipItem = x.reverse()
        this.loading.dismiss()
      });
    this.loading.dismiss()
  }


  showLoading() {
    this.loading = this.loadingCtrl.create({
      spinner: 'hide',
      content: `
        <div class="cssload-container" name="bubbles">
            <div class="cssload-whirlpool"></div>
            <img src="assets/ui/load.png" alt="" width="42">
        </div>
        `,

      cssClass: 'loader',
      showBackdrop: true,
      dismissOnPageChange: false

    });
    this.loading.present();
  }



  // Loading data 
  menusetting(type) {
    let headers = new Headers({
      'Content-Type': 'application/json',
      "Authorization": 'JWT ' + localStorage.getItem('Token')
    });
    let option = new RequestOptions({
      headers: headers
    });
    let urlApi = 'http://www.db.grad.nu.ac.th/apps/ws/getEnglishInfo/' + localStorage.getItem('UserId')
    this.http.post(urlApi, null, option).map(x => x.json()).subscribe(data => {
      this.setload(type)
      if (data.result[0] != undefined) {
        if (data.result[0].pass_test == true) {
          this.engStatus = 1
          if (type == 0) {
            this.percent = this.percent + 14
          } else if (type == 1) {
            this.percent = this.percent + 16.7
          } else if (type == 2) {
            this.percent = this.percent + 20
          }
        } else {
          this.engStatus = 0
        }

      } else {

      }

    }, error => {
      console.log(error);

      this.loading.dismiss();
    })
  }
  gpax() {
    if (this.platform.is('ipad')) {
      let myModal = this.modalCtrl.create(GpaxPage);
      myModal.present();
    } else {
      let myModal = this.modalCtrl.create(GpaxPage);
      myModal.present();
    }
  }
  advisorChecklist(type) {
    let headers = new Headers({
      'Content-Type': 'application/json',
      "Authorization": 'JWT ' + localStorage.getItem('Token')
    });

    let option = new RequestOptions({
      headers: headers
    });
    let urlApi = 'http://www.db.grad.nu.ac.th/apps/ws/getAdvisorAssignStudInfo/' + localStorage.getItem('UserId')
    this.http.post(urlApi, null, option).map(x => x.json()).subscribe(data => {
      let advisorUrl = 'http://www.db.grad.nu.ac.th/apps/ws/getGradStaff/' + data.result[0].advisor_id
      this.http.post(advisorUrl, null, option)
        .map(x => x.json())
        .subscribe(snapshot => {
          this.setload(type)

          if (snapshot.error != null || snapshot.error != undefined) {
            this.advisorStatus = 0
          } else {
            this.advisorStatus = 1
            if (type == 0) {
              this.percent = this.percent + 14
            } else if (type == 1) {
              this.percent = this.percent + 16.7
            } else if (type == 2) {
              this.percent = this.percent + 40
            }
          }

        }, error => {
          console.log(error);

          this.loading.dismiss();
        })
    });
  }
  alertNotification() {
    this.firebaseProvider
      .notificationAlert()
      .subscribe(snap => {

        this.count1 = 0;
        var ele = snap;
        ele.forEach(element => {


          firebase.database()
            .ref('UserInfo/' + localStorage.getItem('UserId'))
            .on('value', snap2 => {
              if (snap2.val().first_login <= element.time) {
                var key = element.read
                if (
                  key[localStorage.getItem('UserId')] == undefined
                  || key[localStorage.getItem('UserId')] == null) {
                  this.count1++;
                }
              } else {


              }
            });
          this.resultNotification()
        })

        this.firebaseProvider
          .notificationPrivate()
          .subscribe(
            snap => {

              this.count2 = 0;
              snap.forEach(element => {
                firebase.database()
                  .ref('UserInfo/' + localStorage.getItem('UserId'))
                  .on('value', snap2 => {
                    if (snap2.val().first_login <= element.time) {
                      var key = element.read
                      if (
                        key[localStorage.getItem('UserId')] == undefined ||
                        key[localStorage.getItem('UserId')] == null) {

                        this.count2++;

                      }
                    } else { }
                  });
              });

              this.resultNotification()
            }, error => {
              console.log(error);

              this.loading.dismiss();
            }
          )
      });

  }
  resultNotification() {
    this.result = this.count1 + this.count2;
  }

  Qualification(type) {
    let urlApi = 'http://www.db.grad.nu.ac.th/apps/ws/getQEInfo/' + localStorage.getItem('UserId')
    let headers = new Headers({ 'Content-Type': 'application/json', "Authorization": 'JWT ' + localStorage.getItem('Token') });
    let option = new RequestOptions({ headers: headers });
    this.http.post(urlApi, null, option)
      .map(x => x.json())
      .subscribe(data => {

        this.setload(type)
        if (data.result[0].result) {
          this.qualification = 1;
          if (type == 0) {
            this.percent = this.percent + 14
          } else if (type == 1) {
            this.percent = this.percent + 16.7
          } else if (type == 2) {
            this.percent = this.percent + 40
          }
        }

      }, error => {
        console.log(error);

        this.loading.dismiss();
      });
  }

  ThesisExam(type) {
    let urlApi = 'http://www.db.grad.nu.ac.th/apps/ws/getExamInfo/' + localStorage.getItem('UserId')
    let headers = new Headers({ 'Content-Type': 'application/json', "Authorization": 'JWT ' + localStorage.getItem('Token') });
    let option = new RequestOptions({ headers: headers });
    this.http.post(urlApi, null, option)
      .map(x => x.json())
      .subscribe(data => {
        this.setload(type)
        if (data.result[0].exam_date) {
          this.thesisexam = 1
          if (type == 0) {
            this.percent = this.percent + 14
          } else if (type == 1) {
            this.percent = this.percent + 16.7
          } else if (type == 2) {
            this.percent = this.percent + 40
          }
        }


      }, error => {
        console.log(error);
        this.loading.dismiss();
      });
  }
  Publication(type) {
    let urlApi = 'http://www.db.grad.nu.ac.th/apps/ws/getPublicationInfo/' + localStorage.getItem('UserId')
    let headers = new Headers({ 'Content-Type': 'application/json', "Authorization": 'JWT ' + localStorage.getItem('Token') });
    let option = new RequestOptions({ headers: headers });
    this.http.post(urlApi, null, option)
      .map(x => x.json())
      .subscribe(data => {
        this.setload(type)
        if (data.result[0].publication_date) {
          this.publication = 1;
          if (type == 0) {
            this.percent = this.percent + 14
          } else if (type == 1) {
            this.percent = this.percent + 16.7
          } else if (type == 2) {
            this.percent = this.percent + 40
          }
        }

      }, error => {
        console.log(error);
        this.loading.dismiss();
      });
  }
  getPermitInfo(type) {
    let urlApi = 'http://www.db.grad.nu.ac.th/apps/ws/getPermitInfo/' + localStorage.getItem('UserId')
    let headers = new Headers({ 'Content-Type': 'application/json', "Authorization": 'JWT ' + localStorage.getItem('Token') });
    let option = new RequestOptions({ headers: headers });
    this.http.post(urlApi, null, option)
      .map(x => x.json())
      .subscribe(data => {
        this.setload(type)
        if (data.result[0].permit_date) {
          this.PermitInfo = 1;
          if (type == 0) {
            this.percent = this.percent + 14
          } else if (type == 1) {
            this.percent = this.percent + 16.7
          } else if (type == 2) {
            this.percent = this.percent + 40
          }
        }

      }, error => {
        console.log(error);
        this.loading.dismiss();
      });
  }
  getCompleteInfo(type) {

    let urlApi = 'http://www.db.grad.nu.ac.th/apps/ws/getCompleteInfo/' + localStorage.getItem('UserId')
    let headers = new Headers({ 'Content-Type': 'application/json', "Authorization": 'JWT ' + localStorage.getItem('Token') });
    let option = new RequestOptions({ headers: headers });
    this.http.post(urlApi, null, option)
      .map(x => x.json())
      .subscribe(data => {
        this.setload(type)
        if (data.result[0].thesiscomplete_date) {
          this.getCompleteInfo_value = 1;
          if (type == 0) {
            this.percent = this.percent + 14
          } else if (type == 1) {
            this.percent = this.percent + 16.7
          } else if (type == 2) {
            this.percent = this.percent + 40

          }
        }


      }, error => {
        console.log(error);

        this.loading.dismiss();
      });

  }

  thesis() {
    let myModal = this.modalCtrl.create(ThesisPage, { 'sum': this.percent }).present();

  }

  eng(status) {
    this.showLoading();
    if (this.platform.is('ipad')) {
      let myModal = this.modalCtrl.create(EngPage, {
        'data': status
      });
      this.loading.dismiss();
      myModal.present();
    } else {
      let myModal = this.modalCtrl.create(EngPage, {
        'data': status
      });
      this.loading.dismiss();
      myModal.present();
    }

  }
  decode(time) {
    var d = new Date(time);
    var date = d.getDate();
    var Month = d.getMonth();
    var year = d.getFullYear();

    var hr = d.getHours();
    var min = d.getMinutes();
    var sec = d.getSeconds()

    var thmonth = new Array("มกราคม", "กุมภาพันธ์", "มีนาคม",
      "เมษายน", "พฤษภาคม", "มิถุนายน", "กรกฎาคม", "สิงหาคม", "กันยายน",
      "ตุลาคม", "พฤศจิกายน", "ธันวาคม");

    return date + " " + thmonth[Number(Month)] + " " + Number(year + 543)

  }
  news(Event) {
    let myModal = this.modalCtrl.create(AnnounceviewPage, { 'object': Event });
    myModal.present();
  }
  showevent(status) {


    let modal = this.modalCtrl.create(CalendarEventViewsPage, { "items": status })
    modal.present()


  }
  response_Err(index) {
    if (index == 1 && this.iload != true) {
      this.firebaseProvider.presentAlert('กรุณาตรวจสอบอินเทอร์เน็ต', 'การโหลดข้อมูลไม่สมบูรณ์')
      this.firebaseProvider.stopLoading()
    }
  }
  setstatus() {
    let j = 1
    this.timeInterval_main = setInterval(() =>
      this.response_Err(j++), 5000);
    this.firebaseProvider.showLoading()
    let urlApi = 'http://www.db.grad.nu.ac.th/apps/ws/get_program/' + localStorage.getItem('types')
    let headers = new Headers({ 'Content-Type': 'application/json', "Authorization": 'JWT ' + localStorage.getItem('Token') });
    let option = new RequestOptions({ headers: headers });
    this.http.post(urlApi, null, option).map(x => x.json()).subscribe(data => {
      this.iload = true

      var type = data.result[0].PROGRAMVERSION_N
      var status = type;
      var text = data.result[0].PROGRAMNAMECERTIFY
      var text2 = text.split("สาขาวิชา");
      this.userid.ProgramName = text2[1]

      if (status == "แผน ก  แบบ ก 1" ||
        status == "แผน ก  แบบ ก 2") {
        this.Publication(1);//9urb,rB
        this.ThesisExam(1);//สอบจบ
        this.getPermitInfo(1);
        this.getCompleteInfo(1);
        this.advisorChecklist(1);//ที่ปรักษา
        this.menusetting(1);//ภาษาอังกฤษ


        let i = 1
        this.timeInterval = setInterval(() =>

          this.reponseErr(1, i++)
          , 5000);
      } else if (status == "แผน ข") {
        this.Publication(2);
        this.advisorChecklist(2);
        this.menusetting(2);

        let i = 1
        this.timeInterval = setInterval(() =>

          this.reponseErr(2, i++)
          , 5000);
      }
      else if (status == "แบบ 1.1"
        || status == "แบบ 1.2"
        || status == "แบบ 2.1"
        || status == "แบบ 2.2"
      ) {

        this.Qualification(0)
        this.Publication(0);
        this.ThesisExam(0);

        this.getPermitInfo(0);

        this.getCompleteInfo(0);
        this.advisorChecklist(0);
        this.menusetting(0);


        let i = 1
        this.timeInterval = setInterval(() =>
          this.reponseErr(0, i++)
          , 5000);
      }

    }, error => {
      console.log(error);
      this.firebaseProvider.stopLoading()
      this.firebaseProvider.presentAlert('กรุณาตรวจสอบอินเทอร์เน็ต', 'การโหลดข้อมูลไม่สมบูรณ์')
      this.loading.dismiss();
    });

  }

  reponseErr(type, time) {
    console.log(type)
    if (type == 0) {
      if (this.setloads != 7) {
        if (time == 2) {
          this.firebaseProvider.presentAlert('กรุณาตรวจสอบอินเทอร์เน็ต', 'การโหลดข้อมูลไม่สมบูรณ์')
          clearInterval(this.timeInterval)
          this.firebaseProvider.stopLoading()
        }

      } else {
        clearInterval(this.timeInterval)
        this.firebaseProvider.stopLoading()
      }
    } else if (type == 1) {

      if (this.setloads != 6) {

        if (time == 2) {
          this.firebaseProvider.presentAlert('กรุณาตรวจสอบอินเทอร์เน็ต', 'การโหลดข้อมูลไม่สมบูรณ์')
          clearInterval(this.timeInterval)
          this.firebaseProvider.stopLoading()
        }
      } else {
        clearInterval(this.timeInterval)
        this.firebaseProvider.stopLoading()
      }
    } else if (type == 2) {
      if (this.setloads != 3) {
        if (time == 2) {
          this.firebaseProvider.presentAlert('กรุณาตรวจสอบอินเทอร์เน็ต', 'การโหลดข้อมูลไม่สมบูรณ์')
          clearInterval(this.timeInterval)
          this.firebaseProvider.stopLoading()
        }
      } else {
        clearInterval(this.timeInterval)
        this.firebaseProvider.stopLoading()
        console.log('success')
      }
    }
  }

  one_login() {

    firebase.database().ref('UserInfo/' + localStorage.getItem('UserId') + '/OnesignalKey').on('value', snap => {

      if (localStorage.getItem('playerId')) {
        if (snap.val().userId != localStorage.getItem('playerId')) {
          this.logout()
        } else {

        }
      }
    })
  }
  logout() {
    localStorage.removeItem('UserId');
    localStorage.removeItem('Token');
    location.reload()
  }
  calendar_more() {
    let modal = this.modalCtrl.create(ModalcalendarPage).present()
  }
  CovertNumber(data) {
    var num = Number(data).toFixed(2)
    if (data == 0 || data == '' || data == '-' || data == undefined || data == null || data == '0') {
      return ''
    }
    return num
  }
  thispercent(data) {
    if (data > 100) {
      return 100
    } else {
      return data
    }
  }
  setload(type) {
    if (type == 0) {
      this.setloads = this.setloads + 1
      if (this.setloads == 7) {
        this.firebaseProvider.stopLoading()
      }
    } else if (type == 1) {
      this.setloads = this.setloads + 1
      if (this.setloads == 6) {
        this.firebaseProvider.stopLoading()
      }
    } else if (type == 2) {
      this.setloads = this.setloads + 1
      if (this.setloads == 3) {
        this.firebaseProvider.stopLoading()
      }
    }
    console.log(this.setloads)
  }
}
