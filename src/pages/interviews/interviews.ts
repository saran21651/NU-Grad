import { Component } from '@angular/core';
import { NavController, NavParams ,Slides} from 'ionic-angular';
import { ViewChild } from '@angular/core';
import { HomePage } from '../home/home'
import { LoginPage } from '../login/login';
declare var $: any;
/**
 * Generated class for the InterviewsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-interviews',
  templateUrl: 'interviews.html',
})
export class InterviewsPage {
  @ViewChild(Slides) slides: Slides;
  slider:any=true;
  setting=this.navParams.get('setting')
  data=this.navParams.get('studentInfo')
  constructor(public navCtrl: NavController,
             public navParams: NavParams,
         
            ) {
             
  }

  ionViewDidLoad() {
   
 
    console.log('ionViewDidLoad InterviewsPage');
  }
  SlideChange(event){
   
    console.log(event._snapIndex)
    // if(event._snapIndex==0){
    //   this.slides.lockSwipeToPrev(true)
    // }else{
    //   this.slides.lockSwipeToPrev(false)
    // }
    $(".draw-arrow2").addClass("active");
    if(event._snapIndex==7){
      this.slider=false
      // this.slides.lockSwipeToNext(true)
    }else{
      this.slider=true
      // this.slides.lockSwipeToNext(false)
    }
    if(event._snapIndex==1){
      $(".draw-arrow1").addClass("active");
    }else{
      $(".draw-arrow1").removeClass("active");
    }
    if(event._snapIndex==2){
      $(".draw-arrow2").addClass("active");
    }else{
      $(".draw-arrow2").removeClass("active");
    }
    if(event._snapIndex==3){
      $(".draw-arrow3").addClass("active");
    }else{
      $(".draw-arrow3").removeClass("active");
    }
  }
  drag(event){
    if(event._snapIndex==1){
      $(".draw-arrow1").addClass("active");
    }else{
      $(".draw-arrow1").removeClass("active");
    }
    if(event._snapIndex==2){
      $(".draw-arrow2").addClass("active");
    }else{
      $(".draw-arrow2").removeClass("active");
    }
    if(event._snapIndex==3){
      $(".draw-arrow3").addClass("active");
    }else{
      $(".draw-arrow3").removeClass("active");
    }

  }
  particel(){
    
  }
  skip(){
    console.log('skip')
    this.slides.slideTo(7, 1000);
  }
  gohome(){
    if(this.setting!=true){
      localStorage.setItem('fist_user','true')
      this.navCtrl.push(LoginPage)
    }else{
      // this.navCtrl.push(HomePage)
      this.navCtrl.popTo(HomePage)
    }
   
  }
}
