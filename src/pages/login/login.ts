import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, Loading, AlertController, ToastController, ModalController } from 'ionic-angular';

import { HomePage } from '../home/home';
import * as firebase from 'firebase';
import { Http, Headers, RequestOptions } from '@angular/http'
import 'rxjs/add/operator/map'
import { Storage } from '@ionic/storage';
declare var jquery: any;
declare var $: any;
import { FirebaseListObservable } from 'angularfire2/database';
import { FirebaseProvider } from './../../providers/firebase/firebase';
import { Network } from '@ionic-native/network';
import { CameraPage } from '../camera/camera';
import { GuestsPage } from '../guests/guests';

@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  user = {
    "username": "",
    "password": ""
  }
  loading: Loading;
  shoppingItems: FirebaseListObservable<any[]>;
  userId: any
  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public http: Http,
    public loadingCtrl: LoadingController,
    private storage: Storage,
    public alertCtrl: AlertController,
    public firebaseProvider: FirebaseProvider,
    public network: Network,
    public toastCtrl: ToastController,
    public modalCtrl: ModalController
  ) {
    // watch network for a connection

  }

  ionViewDidLoad() {

  }
  ionViewWillEnter() {




    // this.showLoading()
    // this.loading.dismiss();

    this.network.onDisconnect().subscribe(data => {

      this.InternetDisconnect()

    }, error => console.error(error));



    this.network.onConnect().subscribe(data => {
      this.loading.dismiss();
    }, err => console.error(err)

    );
  }







  showLoading() {
    this.loading = this.loadingCtrl.create({
      spinner: 'hide',
      content: `
        <div class="cssload-container" name="bubbles">
            <div class="cssload-whirlpool"></div>
            <img src="assets/ui/load.png" alt="" width="42">
        </div>
        `,

      cssClass: 'loader',
      showBackdrop: true,
      dismissOnPageChange: false

    });
    this.loading.present();

  }
  InternetDisconnect() {
    this.loading = this.loadingCtrl.create({
      spinner: 'hide',
      content: `
        <div class="cssload-container" name="bubbles">
            <div class="cssload-whirlpool"></div>
            <img src="assets/ui/internet.png" alt="" width="42">
        </div>
        `,

      cssClass: 'loader',
      showBackdrop: true,
      dismissOnPageChange: false

    });
    this.loading.present();

  }
  login() {
    this.showLoading()
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let postParam = {
      username: this.user.username,
      password: this.user.password
    }
    if (this.user.username === null || this.user.password === null || this.user.username === '' || this.user.password === '') {
      if (this.user.username === null || this.user.username === '') {
        this.openAlertUsername();
        this.loading.dismiss();
      } else {
        this.openAlertPassword();
        this.loading.dismiss();
      }
    } else {
      let option = new RequestOptions({ headers: headers });
      this.http.post("http://www.db.grad.nu.ac.th/apps/ws/login", postParam, option)
        .map(res => res.json())
        .subscribe(data => {
        if(data.token!=undefined){

          
       
          localStorage.setItem('Token', data.token)                 //setlocalvalue
          
          this.get_student_info(postParam.username,data.token)
        }else{
          this.firebaseProvider.presentAlert('ข้อมูลไม่ถูกต้อง','ตรวจสอบข้อมูลของท่านอีกครั้ง')
          this.loading.dismiss();
        }
          // this.navCtrl.push(HomePage)                        //Next page
        }, error => {
          console.log(error);
          this.openAlertDB()
          this.firebaseProvider.presentAlert('การเชื่อมต่อผิดพลาด','ไม่สามารถเชื่อมต่อกับฐานข้อมูลมหาวิทยาลัยได้')
        
          localStorage.removeItem('UserId');
          localStorage.removeItem('Token');
          this.loading.dismiss();
        });
      // this.navCtrl.push(HomePage)
    }

  }
  get_student_info(uid,token) {
    let urlApi = 'http://www.db.grad.nu.ac.th/apps/ws/get_student_info/' + uid
    let headers = new Headers({ 'Content-Type': 'application/json', "Authorization": 'JWT ' + token });
    let option = new RequestOptions({ headers: headers });
    this.http.post(urlApi, null, option)
      .map(x => x.json())
      .subscribe(data => {
        
        if (data.result.length > 0) {
          
          firebase.database()
            .ref('UserInfo/' + uid + '/FaceID')
            .once('value', snapshot => {

              if (snapshot.val() != undefined || snapshot.val() != null) {
                    if(localStorage.getItem('playerId')){
                      this.firebaseProvider.updatePlayerID(localStorage.getItem('playerId'))
                    }
                    localStorage.setItem('UserId',data.result[0].STUDENTCODE)    
                  localStorage.setItem('types',data.result[0].PROGRAMID)
                  localStorage.setItem('GPA',data.result[0].GPA)
                              
                  this.firebaseProvider.checkin(data.result[0])
                  this.loading.dismiss();
                  this.navCtrl.push(HomePage, { 'studentInfo': data.result[0] })
              } else {
                
                this.navCtrl.push(CameraPage)
    
                this.loading.dismiss();

              }

            })
        } else {
          this.firebaseProvider.presentAlert('การเชื่อมต่อผิดพลาด','ขออภัยระบบไม่รองรับนิสิตระดับปริญญาตรี')

        
          localStorage.removeItem('UserId');
          localStorage.removeItem('Token');
          this.loading.dismiss();
        }

      }, error => {
        console.log(error);
        this.openAlertDB()
        this.loading.dismiss();
      });
  }


  // Alert
  openAlertDB() {
    // this.frostedGlassService.onActivateBackdrop.next(true);
    let alert = this.alertCtrl.create({
      title: 'ไม่พบข้อมูล',
      subTitle: 'กรุณาตรวจสอบข้อมูล',
      buttons: ['OK']
    });
    alert.present();

  }
  Guest() {

    let myModal = this.modalCtrl.create(GuestsPage)
    myModal.present();
    $(".body").addClass("active");
    myModal.onDidDismiss(data => {

      $(".body").removeClass("active");


    });

  }
  openAlertUsername() {
    let alert = this.alertCtrl.create({
      subTitle: 'กรุณาตรวจสอบข้อมูล',
      buttons: ['OK']
    });
    alert.present();

  }
  openAlertPassword() {
  
    let alert = this.alertCtrl.create({
      // title: 'Example',
      subTitle: 'กรุณากรอกรหัสผ่าน',
      buttons: ['OK']
    });
    alert.present();

  }
}