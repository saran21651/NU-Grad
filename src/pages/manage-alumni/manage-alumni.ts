import { Component } from '@angular/core';
import { NavParams, ViewController }  from 'ionic-angular';

@Component({
  templateUrl: 'manage-alumni.html'
})
export class ManageAlumni {
  myParam: string;
  items :Array<string>;
  events :Array<string>;

    public json=["data",]

    displayData = [{
      "text": "item 1",
      "value": 1,
      "img":"https://pbs.twimg.com/profile_images/378800000017423279/1a6d6f295da9f97bb576ff486ed81389_400x400.png"
    },
    {
      "text": "item 2",
      "value": 2,
      "img":"http://my.mobirise.com/data/userpic/764.jpg"
    },
    {
      "text": "item 3",
      "value": 3
    },
    {
      "text": "item 4",
      "value": 4
    },
    {
      "text": "item 5",
      "value": 5
    },
    ];

  constructor(
    public viewCtrl: ViewController,
    params: NavParams
  ) {
    this.myParam = params.get('myParam');
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }


  ngOnInit() {
    this.setItems();
    this.setevent()
  }
  setevent(){

    this.displayData 
  }
  setItems() {
    this.items = ['การยื่นสำเร็จการศึกษา','กิจกรรมอบรมStartUp'];
  }
  



}