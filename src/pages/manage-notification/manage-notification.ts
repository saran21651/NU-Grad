import { Component } from '@angular/core';
import { NavParams, ViewController,ModalController }  from 'ionic-angular';
import {NotificationPage} from '../notification/notification'
@Component({
  templateUrl: 'manage-notification.html'
})
export class ManageNotifiction {
  myParam: string;
  items: Array<string>;
  constructor(
    public viewCtrl: ViewController,
    params: NavParams,
    public modalCtrl: ModalController

  ) {
    this.myParam = params.get('myParam');
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }
  ngOnInit() {
    this.setItems();
  }

  setItems() {
    this.items = ['การยื่นสำเร็จการศึกษา','กิจกรรมอบรมStartUp'];
  }
  NotificationPage(){
    let modal = this.modalCtrl.create(NotificationPage)
   modal.present()
}
  filterItems(ev: any) {
    this.setItems();
    let val = ev.target.value;

    if (val && val.trim() !== '') {
      this.items = this.items.filter(function(item) {
        return item.toLowerCase().includes(val.toLowerCase());
      });
    }
  }
}