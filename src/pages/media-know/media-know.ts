import { Component } from '@angular/core';
import { NavParams, ViewController }  from 'ionic-angular';

@Component({
  templateUrl: 'media-know.html'
})
export class MediaKnow {
  myParam: string;

  constructor(
    public viewCtrl: ViewController,
    params: NavParams
  ) {
    this.myParam = params.get('myParam');
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }
}