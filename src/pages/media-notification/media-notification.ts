import { Component } from '@angular/core';
import { NavParams, ViewController }  from 'ionic-angular';
import {NotificationPage} from '../notification/notification'
import { ModalController } from 'ionic-angular/components/modal/modal-controller';
@Component({
  templateUrl: 'media-notification.html'
})
export class MediaNotifiction {
  myParam: string;
  items: Array<string>;
  constructor(
    public viewCtrl: ViewController,
    params: NavParams,
    public modalCtrl:ModalController
  ) {
    this.myParam = params.get('myParam');
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }
  ngOnInit() {
    this.setItems();
  }

  setItems() {
    this.items = ['สามารถอัพเดตระบบปฎิบัติการ','GradNUapp'];
  }

  NotificationPage(){
    let modal = this.modalCtrl.create(NotificationPage)
   modal.present()
}
  
  filterItems(ev: any) {
    this.setItems();
    let val = ev.target.value;

    if (val && val.trim() !== '') {
      this.items = this.items.filter(function(item) {
        return item.toLowerCase().includes(val.toLowerCase());
      });
    }
  }
}