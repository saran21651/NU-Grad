

/**
 * Generated class for the MenuipadPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
import { Component, ViewChild } from '@angular/core';
import { Nav, Platform ,ViewController,NavController,LoadingController,Loading,AlertController,ModalController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { LoginPage } from '../login/login'
import { NotificationPage } from '../notification/notification';
import { SettingPage }from '../setting/setting';
import { Modalqrcode } from '../modal-qrcode/modal-qrcode';
import { Http, Headers, RequestOptions } from '@angular/http'
import 'rxjs/add/operator/map';
import { VideoPage } from '../video/video'
import {ScholarshipMorePage} from '../scholarship-more/scholarship-more'
//
import * as firebase from 'firebase';
import { Storage } from '@ionic/storage';
import {AnnouncelistPage} from '../announcelist/announcelist'
import {Modalnotification} from '../modal-notification/modal-notification'
import {NotificationBackgroundPage} from '../notification-background/notification-background'
// import { Modalnotification } from '../modal-notification/modal-notification';
import {FirebaseProvider} from './../../providers/firebase/firebase';
import {DtsPage} from '../dts/dts';
import {EventPage} from '../event/event';
import {ResPage} from '../res/res';
import { SurvayPage } from "../survay/survay";
import {ContactPage} from "../contact/contact"
@Component({
  selector: 'page-menuipad',
  templateUrl: 'menuipad.html',
})
export class MenuipadPage {
  @ViewChild(Nav) nav: Nav;
  pages: Array<{title: string, component: any}>
  loading: Loading;
  items: any;

  constructor(
      public platform: Platform,
      public statusBar: StatusBar,
      public splashScreen: SplashScreen ,
      public viewCtrl:ViewController,
      public navCtrl:NavController,
      public loadingCtrl:LoadingController,
      public alertCtrl: AlertController,
      public modalCtrl:ModalController,
      public http:Http,
      public db:FirebaseProvider
    ) {
      this.db.showLoading()
      this.db.stopLoading()
  }
  dismiss() {
    this.viewCtrl.dismiss();
  }

  menu_home(){
    let modal = this.modalCtrl.create(VideoPage);
    modal.present();
  }
  showLoading() {
    this.loading = this.loadingCtrl.create({
        spinner: 'hide',
        content: `
        <div class="cssload-container" name="bubbles">
            <div class="cssload-whirlpool"></div>
            ออกจากระบบ
        </div>
        `,
     
        cssClass: 'loader',
        showBackdrop: true ,
        dismissOnPageChange:false

    });
    this.loading.present();

  }
  setting(){
    let modal = this.modalCtrl.create(SettingPage);
    modal.present();
  
  }

  ngOnInit() {
    this.setItems();
  }

  setItems() {
    this.items = [{
      "title": "Event",
      'url': 'assets/ui/home/menu/new/event.png',
      'component': EventPage
    },
    {
      "title": "News",
      'url': 'assets/ui/home/menu/new/book.png',
      'component': AnnouncelistPage
    },
    {
      "title": "Scholarship",
      'url': 'assets/ui/home/menu/new/scho.png',
      'component': ScholarshipMorePage
    },
    {
      "title": "MOOC",
      'url': 'assets/ui/home/menu/new/camera.png',
      'component': VideoPage
    },
    {
      "title": "DTS",
      'url': 'assets/ui/home/menu/new/res.png',
      'component': DtsPage
    },
    {
      "title": "Notification",
      'url': 'assets/ui/home/menu/new/noti.png',
      'component': NotificationBackgroundPage
    },
    {
      "title": "Evaluation",
      'url': 'assets/ui/home/menu/new/survey.png',
      'component': SurvayPage
    },
    {
      "title": "Contact",
      'url': 'assets/ui/home/menu/new/contact.png',
      'component': ContactPage
    },
    {
      "title": "Setting",
      'url': 'assets/ui/home/menu/new/settingmenu.png',
      'component': SettingPage
    },
 
    ]
  }
  NotificationPage(obj) {
    console.log(obj)
    let modal = this.modalCtrl.create(NotificationPage, {
      'obj': obj
    });
    modal.present();
  }

  filterItems(ev: any) {
    this.setItems();
    let val = ev.target.value;

    if (val && val.trim() !== '') {
      this.items = this.items.filter(function (item) {
        return item.toLowerCase().includes(val.toLowerCase());
      });
    }
  }

  gotopage(ele) {
    this.db.showLoading()
    if (ele == Modalqrcode) {
      this.qrcode()
    }else if (ele == VideoPage){
       this.navCtrl.push(ele)
    }else if(ele == 'Logout'){
      this.logout()
    }
     else if (ele == EventPage) {
      firebase.database().ref('popup').orderByChild('type').equalTo('event').once('value', snapshot => {
        let modal = this.modalCtrl.create(ele, { 'ele': snapshot.val() });
        modal.present();
      })

    }else if(ele == DtsPage){
      this.navCtrl.push(ele)
      // let modal = this.modalCtrl.create(ele)
      // modal.present();


    } else {

      if (this.platform.is('ipad')) {
        this.navCtrl.push(ele)

     
      } else {
        let modal = this.modalCtrl.create(ele);
        modal.present();
        // this.navCtrl.push(ele)

      }

    }
    this.db.stopLoading()
  }
  qrcode() {
    let headers = new Headers({
      'Content-Type': 'application/json',
      "Authorization": 'JWT ' + localStorage.getItem('Token')
    });

    let option = new RequestOptions({
      headers: headers
    });
    let urlApi = 'http://www.db.grad.nu.ac.th/apps/ws/get_student_info/' + localStorage.getItem('UserId')

    this.http.post(urlApi, null, option).map(x => x.json()).subscribe(data => {
      console.log(data)
      // console.log(data)


      let myModal = this.modalCtrl.create(Modalqrcode, {
        'data': data
      });
      myModal.present();
      myModal.onDidDismiss(data => {



      });
    })

    // console.log("qrcode")
    // let myModal = this.modalCtrl.create(Modalqrcode);
    // myModal.present();
  }
  notification() {




    let myModal = this.modalCtrl.create(Modalnotification);
    myModal.present();

    myModal.onDidDismiss(data => {




    });
  }
  logout(){
    
   
    this.presentConfirm()

  }
  presentConfirm() {
    let alert = this.alertCtrl.create({
      title: 'ออกจากระบบ',
      message: 'คุณต้องการจะออกใช่หรือไม่',
      buttons: [
        {
          text: 'ยกเลิก',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'ตกลง',
          handler: () => {
            localStorage.removeItem('UserId');
            localStorage.removeItem('Token');
            this.showLoading()
            location.reload()
          }
        }
      ]
    });
    alert.present();
  }

}
