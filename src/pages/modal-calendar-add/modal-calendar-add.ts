import { Component } from '@angular/core';
import { NavController, NavParams, AlertController, ViewController } from 'ionic-angular';
import { Calendar } from '@ionic-native/calendar';
import { FirebaseProvider } from './../../providers/firebase/firebase';

@Component({
  templateUrl: 'modal-calendar-add.html',
  selector: 'page-modal-calendar-add',
})
export class AddEventPage {

  event = { 'title': "", 'location': "", 'message': "", 'startDate': '', 'endDate': '' };

  constructor(public alertCtrl: AlertController,
    public navCtrl: NavController,
    public navParams: NavParams,
    private calendar: Calendar,
    public viewCtrl: ViewController,
    public db: FirebaseProvider
  ) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AddEventPage');
  }

  save(ele) {
    // alert('บันทึกเรียบร้อย')

    var d = new Date(ele.startDate)
    var d2 = new Date(ele.startDate)
    var offset = new Date().getTimezoneOffset() * 60 * 1000;
    let time = Date.parse(ele.startDate) + offset;




    this.calendar.createEvent(this.event.title, this.event.location, this.event.message, new Date(this.event.startDate), new Date(this.event.endDate)).then(
      (msg) => {

        let alert = this.alertCtrl.create({
          title: 'GradNu!',
          subTitle: 'บันทึกสำเร็จ',
          buttons: ['OK']
        });

        alert.present();


        this.db.addCalendar().object('calendar/' + localStorage.getItem('UserId') + '/' + d.getTime() + '/' + (Date.parse(ele.startDate)+new Date().getTimezoneOffset()*Math.floor(Math.random() * 141))).set({
          "address": ele.location,
          "des": ele.message,
          "end": time,
          "start": time,
          "title": ele.title
        })

        this.navCtrl.pop();
      },
      (err) => {
        let alert = this.alertCtrl.create({
          title: 'Failed!',
          subTitle: err,
          buttons: ['OK']
        });
        console.log(this.calendar)
        alert.present();
      }
    );
  
  }
  dismiss() {
    this.viewCtrl.dismiss();
  }
  calculateTime(offset: any) {
    let d = new Date();
    let nd = new Date(d.getTime() + (3600000 * offset));
    return nd.toISOString();
  }


}


