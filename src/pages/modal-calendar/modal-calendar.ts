import { Component } from '@angular/core';
import {  ViewController,AlertController ,NavController,ModalController}  from 'ionic-angular';
import { Calendar } from '@ionic-native/calendar';
import { Platform } from 'ionic-angular';
import { AddEventPage } from '../modal-calendar-add/modal-calendar-add';
import { FirebaseProvider } from './../../providers/firebase/firebase';
import { CalendarEventPage} from '../calendar-event/calendar-event';
import {CalendarViewPage} from'../calendar-view/calendar-view'
@Component({
  selector: 'page-modal-calendar.html',
  templateUrl: 'modal-calendar.html'
})
export class ModalcalendarPage {

  public press: number = 0;
  public calendarswitch: any;
  public year: number = 543;
  // @ViewChild('myMap') myMap; 
  grad: any
  user: any
  updatedAngle = 0;
  originalAngle = 0;
  currentAngle = 0;
  myParam: string;

  public item: any;
  pet: string = "puppies";
  isAndroid: boolean = false;
  date: any;
  daysInThisMonth: any;
  daysInLastMonth: any;
  daysInNextMonth: any;
  monthNames: string[];
  currentMonth: any;
  currentYear: any;
  currentDate: any;
  eventList: any;
  eventListMonth: any;
  currentDay: any;
  selectedEvent: any;
  isSelected: any;
  days: any;
  currentDates: any;
  currentMonths: any;

  dateIsSelected: any;
  selectedDate: any;
  checkSelect = false;
  start: any;
  scholarshipItem: any
  slides = []
  url;
  newSid = '';
  newPid = '';
  img;
  toggleSubscribe1: boolean;
  toggleSubscribe2: boolean;
  tagsSub: any;
  isDataAvailable = false;
  noti: any;
  userid = {
    'Name': '',
    'Code': '',
    'ProgramName': ''
  }
  DateCurrent: any;
  MouthCurrent: any;
  viewStatus = 0;
  viewStatus2 = 0;
  percent: any = 0;
  engStatus = 0;
  advisorStatus = 0;
  researchHuman: any;
  count1: any = 0;
  count2: any = 0;
  result: any = 0;
  qualification = 0;
  PermitInfo = 0;
  publication = 0;
  thesisexam = 0;
  getCompleteInfo_value = 0;
  InfoEnd: any;
  onlineCalendar: any;
  event_calendar:any;
  items:any
  
  constructor(
    private alertCtrl: AlertController,
    public navCtrl: NavController,
    private calendar: Calendar,
    public platform: Platform,
    public viewCtrl: ViewController,
    public firebaseProvider:FirebaseProvider,
    public modalCtrl:ModalController
  ) {
      this.isAndroid = platform.is('android');  


      this.firebaseProvider.Ofcalendar()
      .subscribe(snapshot => {
        this.onlineCalendar = snapshot


      });

 
  }
    

  ionViewWillEnter() {
    this.date = new Date();
       this.DateCurrent = this.date.getDate()
    this.monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
    this.days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
    this.getDaysOfMonth();
    this.loadEventThisMonth();
    this.currentDates = this.date.getDate()
    this.currentDay = this.days[this.date.getDay()];
    this.currentMonths = this.monthNames[this.date.getMonth()]
    this.calendarswitch = "calendar";
    this.MouthCurrent = this.monthNames[this.date.getMonth()] + " " + (this.date.getFullYear() + 543);
 
  }
  viewEvent() {
 


    let myModal = this.modalCtrl.create(CalendarViewPage)
    myModal.present();

  }
  checkToday(day) {
    let current = new Date();
    let today = new Date(this.date.getFullYear(), (this.date.getMonth()), day);
    let inDay = new Date(current.getFullYear(), (current.getMonth()), this.currentDates);

    // console.log(today);
    // console.log(inDay);
    return ((today.getDate() == inDay.getDate()) && (today.getMonth() == inDay.getMonth()) && (today.getFullYear() == inDay.getFullYear()));
  }

  getDaysOfMonth() {
    this.daysInThisMonth = new Array();
    this.daysInLastMonth = new Array();
    this.daysInNextMonth = new Array();
    this.currentMonth = this.monthNames[this.date.getMonth()];
    this.currentYear = this.date.getFullYear();
    if (this.date.getMonth() === new Date().getMonth()) {
      this.currentDate = new Date().getDate();
    } else {
      this.currentDate = 999;
    }

    var firstDayThisMonth = new Date(this.date.getFullYear(), this.date.getMonth(), 1).getDay();
    var prevNumOfDays = new Date(this.date.getFullYear(), this.date.getMonth(), 0).getDate();
    for (var i = prevNumOfDays - (firstDayThisMonth - 1); i <= prevNumOfDays; i++) {
      this.daysInLastMonth.push(i);
    }

    var thisNumOfDays = new Date(this.date.getFullYear(), this.date.getMonth() + 1, 0).getDate();
    for (var j = 0; j < thisNumOfDays; j++) {
      this.daysInThisMonth.push(j + 1);
    }

    var lastDayThisMonth = new Date(this.date.getFullYear(), this.date.getMonth() + 1, 0).getDay();
    // var nextNumOfDays = new Date(this.date.getFullYear(), this.date.getMonth()+2, 0).getDate();
    for (var k = 0; k < (6 - lastDayThisMonth); k++) {
      this.daysInNextMonth.push(k + 1);
    }
    var totalDays = this.daysInLastMonth.length + this.daysInThisMonth.length + this.daysInNextMonth.length;
    if (totalDays < 36) {
      for (var l = (7 - lastDayThisMonth); l < ((7 - lastDayThisMonth) + 7); l++) {
        this.daysInNextMonth.push(l);
      }
    }
  }
  showevent(status,event,dd,mm,yy) {
    if (status == true) {
      let myModal = this.modalCtrl.create(CalendarEventPage, {
        'Element': event,
        'day': dd,
        'mm': mm,
        'yy': yy
      })
      myModal.present();
      myModal.onDidDismiss(data => { });
    } else { }
    
  }

  dateSelected(day) {
    this.checkSelect = true;
    this.dateIsSelected = new Date(this.date.getFullYear(), (this.date.getMonth() + 1), day);
    //console.log(this.dateIsSelected);
  }

  selected(day) {
    //console.log(day);
    let checkday = new Date(this.date.getFullYear(), (this.date.getMonth() + 1), day);
    // console.log("Check: " + checkday);
    // console.log("Selected: " + this.dateIsSelected);
    if (this.checkSelect) {
      return ((checkday.getDate() == this.dateIsSelected.getDate()) && (checkday.getMonth() == this.dateIsSelected.getMonth()) && (checkday.getFullYear() == this.dateIsSelected.getFullYear()));
    } else {
      return false;
    }
  }

  goToLastMonth() {
    // this.dateIsSelected = '';
    this.date = new Date(this.date.getFullYear(), this.date.getMonth(), 0);
    this.getDaysOfMonth();
    this.loadEventThisMonth();
  }

  goToNextMonth() {
    // this.dateIsSelected = '';
    this.date = new Date(this.date.getFullYear(), this.date.getMonth() + 2, 0);
    this.getDaysOfMonth();
    this.loadEventThisMonth();
    // this.checkEvent(this.date)
  }


  loadEventThisMonth() {

    this.eventListMonth = new Array();
    this.eventList = new Array();
    var startDate = new Date(this.date.getFullYear(), this.date.getMonth() - 1, 1);
    var endDate = new Date(this.date.getFullYear(), this.date.getMonth() + 2, 0);





    if (this.platform.is('android')) {
      /* android */
      this.calendar.findEvent('', '', '', startDate, endDate)
        .then((result) => {
          result.forEach(item => {


            this.eventList.push(item);
            var hasEvent = false;
            var thisDate1 = new Date(this.date.getFullYear() + "-" + (this.date.getMonth() + 1) + "-" + 1 + " 00:00:00");
            var thisDate2 = new Date(this.date.getFullYear() + "-" + (this.date.getMonth() + 1) + "-" + 31 + " 23:59:59");

            let eventDateStart = new Date(new Date(item.startDate).getUTCFullYear(), new Date(item.startDate).getUTCMonth(), new Date(item.startDate).getUTCDate(), new Date(item.startDate).getUTCHours(), new Date(item.startDate).getUTCMinutes(), new Date(item.startDate).getUTCSeconds());
            let eventDateEnd = new Date(new Date(item.endDate).getUTCFullYear(), new Date(item.endDate).getUTCMonth(), new Date(item.endDate).getUTCDate(), new Date(item.endDate).getUTCHours(), new Date(item.endDate).getUTCMinutes(), new Date(item.endDate).getUTCSeconds() - 1);

            if (((eventDateStart >= thisDate1) && (eventDateStart <= thisDate2)) || ((eventDateEnd >= thisDate1) && (eventDateEnd <= thisDate2))) {
              this.eventListMonth.push(item);
            }

          })
        }, (err) => {
          console.log(err);
        }
        );

      /* android */
    } else if (this.platform.is('ios')) {
      this.calendar.listEventsInRange(startDate, endDate).then(
        (msg) => {
          msg.forEach(item => {

            this.eventList.push(item);

          });
        },
        (err) => {
          console.log(err);
        }
      );
    }


  }
  checkEvent(day) {
    var hasEvent = false;

    var thisDate1 = (this.date.getFullYear() + 543) + "-" + (("0" + (this.date.getMonth() + 1)).slice(-2)) + "-" + day + " 00:00:00";
    var thisDate2 = (this.date.getFullYear() + 543) + "-" + (("0" + (this.date.getMonth() + 1)).slice(-2)) + "-" + day + " 23:59:59";
    var x = "2561-05-26 00:00:00";
    var x2 = "2018-05-26 23:59:59";
    var obj: any = {
      "status": "",
      "event": ""
    }
    this.onlineCalendar.forEach(element => {
      var d = new Date(Number(element.$key))
      var Date1 = (d.getFullYear() + 543) + "-" + (("0" + (d.getMonth() + 1)).slice(-2)) + "-" + d.getDate() + " 00:00:00";
      if (thisDate1 == Date1) {
        obj.status = true;
        obj.event = element;
      }
    });
    return obj
  }

  selectDate(day) {
    this.dateSelected(day);
    this.isSelected = false;
    this.selectedEvent = new Array();
    var thisDate1 = new Date(this.date.getFullYear() + "-" + (this.date.getMonth() + 1) + "-" + day + " 00:00:00");
    var thisDate2 = new Date(this.date.getFullYear() + "-" + (this.date.getMonth() + 1) + "-" + day + " 23:59:59");
    this.eventList.forEach(event => {
      let eventDateStart = new Date(new Date(event.startDate).getUTCFullYear(), new Date(event.startDate).getUTCMonth(), new Date(event.startDate).getUTCDate(), new Date(event.startDate).getUTCHours(), new Date(event.startDate).getUTCMinutes(), new Date(event.startDate).getUTCSeconds());
      let eventDateEnd = new Date(new Date(event.endDate).getUTCFullYear(), new Date(event.endDate).getUTCMonth(), new Date(event.endDate).getUTCDate(), new Date(event.endDate).getUTCHours(), new Date(event.endDate).getUTCMinutes(), new Date(event.endDate).getUTCSeconds() - 1);
      if (((eventDateStart >= thisDate1) && (eventDateStart <= thisDate2)) || ((eventDateEnd >= thisDate1) && (eventDateEnd <= thisDate2))) {
        this.isSelected = true;
        this.selectedEvent.push(event);
      }
    });
  }

  changeDate(date) {
    let newDate = ((new Date(date)).getFullYear()) + "-" + ((new Date(date)).getMonth() + 1) + "-" + ((new Date(date)).getDate());
    //var newDate = new Date(date);
    return newDate;
  }

  

  deleteEvent(evt) {
    // console.log(new Date(evt.startDate.replace(/\s/, 'T')));
    // console.log(new Date(evt.endDate.replace(/\s/, 'T')));
    let alert = this.alertCtrl.create({
      title: 'Confirm Delete',
      message: 'Are you sure want to delete this event?',
      buttons: [{
        text: 'Cancel',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      },
      {
        text: 'Ok',
        handler: () => {
          this.calendar.deleteEvent(evt.title, evt.location, evt.notes, new Date(evt.startDate.replace(/\s/, 'T')), new Date(evt.endDate.replace(/\s/, 'T'))).then(
            (msg) => {

              this.loadEventThisMonth();
              // this.selectDate(new Date(evt.startDate.replace(/\s/, 'T')).getDate());
            },
            (err) => {
              console.log(err);
            }
          )
        }
      }
      ]
    });
    alert.present();
  }

  calendarview(item) {

  //   let myModal = this.modalCtrl.create(CalendarViewPage, {
  //     "item": item
  //   });
  //   myModal.present();
  // }
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

}
