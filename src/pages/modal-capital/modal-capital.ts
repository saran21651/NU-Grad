import { Component } from '@angular/core';
import { NavParams, ViewController ,ModalController}  from 'ionic-angular';
import { CapitalPage } from '../capital/capital';

@Component({
  templateUrl: 'modal-capital.html'
})
export class Modalcapital {
  myParam: string;
  items: Array<string>;
  constructor(
    public viewCtrl: ViewController,
    params: NavParams,
    public modalCtrl: ModalController
  ) {
    this.myParam = params.get('myParam');
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }
  ngOnInit() {
    this.setItems();
  }

  setItems() {
    this.items = [  'ทุน วช.'
                    ,'ทุน สกว.'
                  ];
  }
  capitalPage(){
    let myModal = this.modalCtrl.create(CapitalPage,{
      id:"1234",
      name:"CAR"
  });   
    myModal.present();
  }
  filterItems(ev: any) {
    this.setItems();
    let val = ev.target.value;

    if (val && val.trim() !== '') {
      this.items = this.items.filter(function(item) {
        return item.toLowerCase().includes(val.toLowerCase());
      });
    }
  }
}