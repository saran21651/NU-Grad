import { Component } from '@angular/core';
import { NavParams, ViewController ,LoadingController,Loading}  from 'ionic-angular';
import {Http, Headers, RequestOptions} from '@angular/http'
import 'rxjs/add/operator/map'
import {style} from "typestyle";
import { Storage } from '@ionic/storage';
@Component({
  templateUrl: 'modal-info.html'
})
export class ModalInfoPage {
  myParam: string;
  User ={
          'name':"",
          'fac':"",
          'address':"",
          'tesis':"",
          'status':"",
          'Eng':""
          };
  data :any
  result : any
  myObj :any
  success :any
  loading: Loading
  constructor(
    public viewCtrl: ViewController,
    public params: NavParams,
    public http: Http,
    public loadingCtrl: LoadingController,
    private storage: Storage

  ) {
    
    // this.result = JSON.stringify(params.get('data').result[0]);
    // this.myParam  = params.get('data');
    // this.User.name= params.get('data').result[0].PREFIXNAME+""+params.get('data').result[0].STUDENTNAME+"  "+params.get('data').result[0].STUDENTSURNAME;
              
    // this.User.fac = params.get('data').result[0].PROGRAMNAME
    // this.User.address=params.get('data').result[0].HOMEADDRESS +" "+ params.get('data').result[0].HOMEDISTRICT+" "+params.get('data').result[0].HOMEPROVINCE+" "+params.get('data').result[0].HOMEZIPCODE;
    // // this.User.name=this.myParam.STUDENTNAME;

    // // DATA2
    // this.User.tesis =params.get('data2').result[0].title;
    
    // var str = params.get('data3').result[0].thesiscomplete_date;
    //  this.User.status= str.substring(0, 10);

    // this.User.Eng=params.get('data4').result[0].RECORDTYPEDESC;
    // this.progressbar()

  
  } 
  ionViewDidLoad() {
    
    this.get_student_info()
    this.getAdvisorAssignStudInfo()
    this.getCompleteInfo()
    this.getEnglishInfo()
    this.progressbar()

  }

  progressbar(){
    this.success="75";
    
  }
  dismiss() {
    this.viewCtrl.dismiss();
  }
 
  showLoading() {
    this.loading = this.loadingCtrl.create({
        spinner: 'hide',
        content: `
        <div class="cssload-container" name="bubbles">
            <div class="cssload-whirlpool"></div>
            Loadding
        </div>
        `,
     
        cssClass: 'loader',
        showBackdrop: false ,
        dismissOnPageChange:false

    });
    this.loading.present();
    
  }

  get_student_info(){
    let urlApi = 'http://www.db.grad.nu.ac.th/apps/ws/get_student_info/'+localStorage.getItem('UserId')
    let headers = new Headers({'Content-Type': 'application/json',"Authorization":'JWT '+localStorage.getItem('Token')});
    let option = new RequestOptions({headers:headers});
    this.http.post(urlApi,null,option).map(x=>x.json()).subscribe(data=>{
       this.User.name=data.result[0].PREFIXNAME+""+data.result[0].STUDENTNAME+"  "+data.result[0].STUDENTSURNAME;
       this.User.address=data.result[0].HOMEADDRESS +" "+ data.result[0].HOMEDISTRICT+" "+data.result[0].HOMEPROVINCE+" "+data.result[0].HOMEZIPCODE;
    })
  }

  getAdvisorAssignStudInfo(){

    let urlApi = 'http://www.db.grad.nu.ac.th/apps/ws/getAdvisorAssignStudInfo/'+localStorage.getItem('UserId')
    let headers = new Headers({'Content-Type': 'application/json',"Authorization":'JWT '+localStorage.getItem('Token')});
    let option = new RequestOptions({headers:headers});
    this.http.post(urlApi,null,option).map(x=>x.json()).subscribe(data=>{
        this.User.tesis =data.result[0].title;
    })
  }
  getCompleteInfo(){
    let urlApi = 'http://www.db.grad.nu.ac.th/apps/ws/getCompleteInfo/'+localStorage.getItem('UserId')
    let headers = new Headers({'Content-Type': 'application/json',"Authorization":'JWT '+localStorage.getItem('Token')});
    let option = new RequestOptions({headers:headers});
    this.http.post(urlApi,null,option).map(x=>x.json()).subscribe(data=>{
          var str = data.result[0].thesiscomplete_date;
          this.User.status= str.substring(0, 10);
    })
  }

  getEnglishInfo(){
    let urlApi = 'http://www.db.grad.nu.ac.th/apps/ws/getEnglishInfo/'+localStorage.getItem('UserId')
    let headers = new Headers({'Content-Type': 'application/json',"Authorization":'JWT '+localStorage.getItem('Token')});
    let option = new RequestOptions({headers:headers});
    this.http.post(urlApi,null,option).map(x=>x.json())
    .subscribe(data=>{
          this.User.Eng=data.result[0].RECORDTYPEDESC;
          
    })
  }
}

