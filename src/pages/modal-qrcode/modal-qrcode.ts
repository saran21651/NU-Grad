import { Component } from '@angular/core';
import { NavParams, ViewController } from 'ionic-angular';
import { Http, Headers, RequestOptions } from '@angular/http'
declare var $: any;
@Component({
  templateUrl: 'modal-qrcode.html',
  selector: 'page-modal-qrcode',
})
export class Modalqrcode {
  myParam: string;
  items: any
  
  public User = { "id": "", "name": "", "qrcode": "", "fac": "" }
  constructor(
    public viewCtrl: ViewController,
    params: NavParams,
    public http: Http
  ) {

    this.User.fac = params.get('type')
    this.loaddata()
  }

  dismiss() {
    this.viewCtrl.dismiss()
  }
  flip(data) {
    if (data == true) {
      $(".card-qrcode").addClass('flip')
    } else {
      $(".card-qrcode").removeClass('flip')
    }

  }
  loaddata() {
    let headers = new Headers({
      'Content-Type': 'application/json',
      "Authorization": 'JWT ' + localStorage.getItem('Token')
    });

    let option = new RequestOptions({
      headers: headers
    });
    let urlApi = 'http://www.db.grad.nu.ac.th/apps/ws/get_student_info/' + localStorage.getItem('UserId')

    this.http.post(urlApi, null, option).map(x => x.json()).subscribe(data => {
      console.log(data.result[0])
      this.User.id = data.result[0].STUDENTCODE;
      this.User.name = data.result[0].STUDENTNAME + " " + data.result[0].STUDENTSURNAME;
      this.User.qrcode = data.result[0].STUDENTCODE;
      // this.User.fac = params.get('type')

    });
  }
}


// this.showLoading()
// let headers = new Headers({
//   'Content-Type': 'application/json',
//   "Authorization": 'JWT ' + localStorage.getItem('Token')
// });

// let option = new RequestOptions({
//   headers: headers
// });
// let urlApi = 'http://www.db.grad.nu.ac.th/apps/ws/get_student_info/' + localStorage.getItem('UserId')

// this.http.post(urlApi, null, option).map(x => x.json()).subscribe(data => {
//   if (this.platform.is('ipad')) {
//     this.navCtrl.push(Modalqrcode, {
//       'data': data,
//       'type': this.userid.ProgramName
//     })
//     this.loading.dismiss();
//   } else {
//     this.loading.dismiss();
//     let myModal = this.modalCtrl.create(Modalqrcode, {
//       'data': data,
//       'type': this.userid.ProgramName
//     });
//     myModal.present();


//     $(".body").addClass("active");
//     myModal.onDidDismiss(data => {

//       $(".body").removeClass("active");


//     });
//   }
// })