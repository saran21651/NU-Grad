import { Component } from '@angular/core';
import { NavParams, ViewController }  from 'ionic-angular';

//firebase
import { FirebaseListObservable } from 'angularfire2/database';
import { FirebaseProvider } from './../../providers/firebase/firebase';
@Component({
  templateUrl: 'modal-talk.html'
})
export class Modaltalk {
  myParam: string;
  items: any;
  user:any;
  shoppingItems: FirebaseListObservable<any[]>;
  text={
    "time":"04.00pm",
    "message":"",
    "name":""
  
  }
  constructor(
    public viewCtrl: ViewController,
    params: NavParams,
    public firebaseProvider: FirebaseProvider
  ) {
    this.user=localStorage.getItem('UserId')
    // this.shoppingItems = this.firebaseProvider.getShoppingItems();
    
    console.log(this.user)
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }
  SendMessage(){
    this.text={
      "time":"04.00pm"    , 
      "message":this.text.message,
      "name":this.user
    }

    // this.firebaseProvider.addItem( this.text);
    this.text.message=""
  }
}