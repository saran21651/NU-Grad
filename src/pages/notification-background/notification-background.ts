import {
  Component
} from '@angular/core';
import {
  NavParams,
  ViewController,
  ModalController,
  LoadingController,
  Loading,
  NavController
} from 'ionic-angular';
import {
  NotificationPage
} from '../notification/notification';

import * as firebase from 'firebase';
import {
  FirebaseListObservable
} from 'angularfire2/database';
import {
  FirebaseProvider
} from './../../providers/firebase/firebase';



@Component({
  selector: 'page-notification-background',
  templateUrl: 'notification-background.html',
})
export class NotificationBackgroundPage {
  myParam: string;
  items: any=[];
  loading: Loading;
  localhost: any;
  constructor(
    public viewCtrl: ViewController,
    params: NavParams,
    public modalCtrl: ModalController,
    public firebaseProvider: FirebaseProvider,
    public loadingCtrl: LoadingController,
    public navCtrl: NavController
  ) {
    this.myParam = params.get('myParam');
    this.localhost = localStorage.getItem('Userid')
    this.loadding();
  }


  loadding() {
    this.showLoading()
    firebase.database()
        .ref('UserInfo/'+localStorage.getItem('UserId'))
            .once('value',snap0=>{
          
              console.log(snap0.val().first_login)
    this.firebaseProvider.getnotification()
      .subscribe(snap => {
        this.firebaseProvider.ResultNotification()
          .subscribe(snap2 => {
            var obj = []
            this.items=[]
            
            snap.forEach(element => {
              
              if(snap0.val().first_login<=element.time){
                obj.push(element)
              }else{


              }
            
            });
            snap2.forEach(element => {
              

              if(snap0.val().first_login<=element.time){
                obj.push(element)
              }else{


              }
            });

            this.items = obj
            this.Sort()
          });
          console.log(this.items)
      });
    })

    this.loading.dismiss()
  }
  showLoading() {
    this.loading = this.loadingCtrl.create({
      spinner: 'hide',
      content: `
        <div class="cssload-container" name="bubbles">
            <div class="cssload-whirlpool"></div>
            <img src="assets/ui/load.png" alt="" width="42">
        </div>
        `,

      cssClass: 'loader',
      showBackdrop: true,
      dismissOnPageChange: false

    });
    this.loading.present();

  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

  notofication(item) {

    let status
    let BreakException = {}
    try {
      Object.keys(item).forEach(function (el) {

        if (el == localStorage.getItem('UserId')) {

          status = 1
          throw BreakException;
        } else {
          status = 0
        }
      });
    } catch (e) {
      if (e !== BreakException) throw e;
    }
    return status
  }
  NotificationPage(data) {
    let modal = this.modalCtrl.create(NotificationPage, {
      'data': data
    })
    modal.present();
    if (data.type == "all") {
      this.firebaseProvider.readnotificationAll(data, localStorage.getItem('UserId'))

    } else if (data.type == "person") {
      this.firebaseProvider.readnotification(data, localStorage.getItem('UserId'))
    }


  }
  filterItems(ev: any) {

    let val = ev.target.value;

    if (val && val.trim() !== '') {
      this.items = this.items.filter(function (item) {
        return item.toLowerCase().includes(val.toLowerCase());
      });
    }
  }


  
  decode(time) {
    var d = new Date(time);
    var date = d.getDate();
    var Month = d.getMonth();
    var year = d.getFullYear();

    var hr = d.getHours();
    var min = d.getMinutes();
    var sec = d.getSeconds()
    var thmonth = new Array ("มกราคม","กุมภาพันธ์","มีนาคม",
    "เมษายน","พฤษภาคม","มิถุนายน", "กรกฎาคม","สิงหาคม","กันยายน",
    "ตุลาคม","พฤศจิกายน","ธันวาคม");
    return date + " " + thmonth[Number(Month)] + " " + Number(year+543) + ", เวลา " + hr + ":" + this.min(min)
  }
  min(data){
    if(data<10){
      return "0"+data;
    }else{
      return data
    }
  }
  doRefresh(refresher) {
    console.log('Begin async operation', refresher);

    setTimeout(() => {
      this.loadding()
      refresher.complete();
    }, 2000);
  }
  Sort() {

    this.items.sort(function (a, b) { return a.time - b.time });
    this.items.reverse()

  }

}
