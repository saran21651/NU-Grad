import { Component } from '@angular/core';
import { NavController, NavParams ,ViewController,Loading,LoadingController} from 'ionic-angular';
import { SocialSharing } from '@ionic-native/social-sharing';
/**
 * Generated class for the NotificationPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-notification',
  templateUrl: 'notification.html',
})
export class NotificationPage {
  data:any
  notificattion:any
  loading:Loading
  constructor(public navCtrl: NavController, public navParams: NavParams ,public viewCtrl :ViewController,private socialSharing: SocialSharing,   public loadingCtrl: LoadingController,) {
    // this.showLoading()
    this.notificattion=navParams.get('data')
    // this.loading.dismiss()
  }

  ionViewDidLoad() {
  
  }
  dismiss() {
    this.viewCtrl.dismiss();
  }


  showLoading() {
    this.loading = this.loadingCtrl.create({
        spinner: 'hide',
        content: `
        <div class="cssload-container" name="bubbles">
            <div class="cssload-whirlpool"></div>
            <img src="assets/ui/load.png" alt="" width="42">
        </div>
        `,
     
        cssClass: 'loader',
        showBackdrop: true ,
        dismissOnPageChange:false

    });
    this.loading.present();

  }
  decode(time) {
    var d = new Date(time);
    var date = d.getDate();
    var Month = d.getMonth();
    var year = d.getFullYear();

    var hr = d.getHours();
    var min = d.getMinutes();
    var sec = d.getSeconds()
    var thmonth = new Array ("มกราคม","กุมภาพันธ์","มีนาคม",
    "เมษายน","พฤษภาคม","มิถุนายน", "กรกฎาคม","สิงหาคม","กันยายน",
    "ตุลาคม","พฤศจิกายน","ธันวาคม");
    return date + " " + thmonth[Number(Month)] + " " + Number(year+543) + ", เวลา " + hr + ":" + this.min(min)
  }
  min(data){
    if(data<10){
      return "0"+data;
    }else{
      return data
    }
  }

}
