import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import {FirebaseProvider} from './../../providers/firebase/firebase';

/**
 * Generated class for the QuestionnairePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-questionnaire',
  templateUrl: 'questionnaire.html',
})
export class QuestionnairePage {
  items:any=[]
  constructor(public navCtrl: NavController, public navParams: NavParams, public db:FirebaseProvider) {

    this.db.qurestion().subscribe(x=>{
        this.items=x
     });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad QuestionnairePage');
  }

}
