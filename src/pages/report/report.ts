import { Component } from '@angular/core';
import { NavController, NavParams, ViewController } from 'ionic-angular';

/**
 * Generated class for the ReportPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
declare var jquery: any;
declare var $: any;
@Component({
  selector: 'page-report',
  templateUrl: 'report.html',
})
export class ReportPage {
  cards: any;
    category: string = 'gear';
  constructor(public navCtrl: NavController, public navParams: NavParams,
    public viewCtrl: ViewController
  ) {
    this.cards = new Array(10);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ReportPage');


  }
  dismiss() {
    this.viewCtrl.dismiss()

  }
  /* when document is ready*/

}
