import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the ReseachPaperComPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-reseach-paper-com',
  templateUrl: 'reseach-paper-com.html',
})
export class ReseachPaperComPage {
  items: Array<string>;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ReseachPaperTechPage');
  }
  ngOnInit() {
    this.setItems();
  }

  setItems() {
    this.items = ['ภาษาใหม่','สังคมวันนี้'];
  }
  
  filterItems(ev: any) {
    this.setItems();
    let val = ev.target.value;

    if (val && val.trim() !== '') {
      this.items = this.items.filter(function(item) {
        return item.toLowerCase().includes(val.toLowerCase());
      });
    }
  }
}
