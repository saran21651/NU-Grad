import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the ReseachPaperTechPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-reseach-paper-tech',
  templateUrl: 'reseach-paper-tech.html',
})
export class ReseachPaperTechPage {
  items: Array<string>;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ReseachPaperTechPage');
  }
  ngOnInit() {
    this.setItems();
  }

  setItems() {
    this.items = ['Clound create','AI and Anatomy'];
  }
  
  filterItems(ev: any) {
    this.setItems();
    let val = ev.target.value;

    if (val && val.trim() !== '') {
      this.items = this.items.filter(function(item) {
        return item.toLowerCase().includes(val.toLowerCase());
      });
    }
  }
}
