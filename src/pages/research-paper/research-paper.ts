import { Component } from '@angular/core';
import { NavParams, ViewController }  from 'ionic-angular';

@Component({
  templateUrl: 'research-paper.html'
})
export class ResearchPaper {
  myParam: string;

  constructor(
    public viewCtrl: ViewController,
    params: NavParams
  ) {
    this.myParam = params.get('myParam');
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }
}