import { Component } from '@angular/core';
import { NavController, NavParams, ViewController, ModalController, LoadingController, Loading } from 'ionic-angular';
import { ScholarshipViewPage } from '../scholarship-view/scholarship-view';



import { FirebaseListObservable } from 'angularfire2/database';
import { FirebaseProvider } from './../../providers/firebase/firebase';
declare var jquery: any;
declare var $: any;
@Component({
  selector: 'page-scholarship-more',
  templateUrl: 'scholarship-more.html',
})
export class ScholarshipMorePage {
  loading: Loading;
  number = 6;
  items2: any;
  Infi_load=false
  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController
    , public modalCtrl: ModalController,
    public firebaseProvider: FirebaseProvider,
    public loadingCtrl: LoadingController,
  ) {
  }
  items: any
  ionViewDidLoad() {
    console.log('ionViewDidLoad ScholarshipMorePage');
    this.firebaseProvider.showLoading()
    this.firebaseProvider.getscholarship(this.number)
      .subscribe(snapshot => {
        this.items = snapshot.reverse()
        this.firebaseProvider.stopLoading()
      });
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }
  scholarship(object) {
    // this.navCtrl.push(ScholarshipViewPage,{'object':object})
    let myModal = this.modalCtrl.create(ScholarshipViewPage, { 'object': object });
    myModal.present();
    // console.log("Views")
  }


  showLoading() {
    this.loading = this.loadingCtrl.create({
      spinner: 'hide',
      content: `
        <div class="cssload-container" name="bubbles">
            <div class="cssload-whirlpool"></div>
            <img src="assets/ui/load.png" alt="" width="42">
        </div>
        `,

      cssClass: 'loader',
      showBackdrop: true,
      dismissOnPageChange: false,


    });
    this.loading.present();

  }
  load() {
    this.firebaseProvider.getscholarship(this.number)
      .subscribe(snapshot => {
        this.items2 = snapshot
        this.sort2()
        if(this.items2.length==this.items.length){
          this.Infi_load=true;
          console.log(this.Infi_load)
        }
        for (let index = 0; index < this.items2.length; index++) {
          const element = this.items2[index];
          if (index >= this.items.length) {
            this.items.push(element)
          }
        }
      
      });
  }
  sort() {

    this.items.sort(function (a, b) { return a.time - b.time });
    this.items.reverse()

  }
  sort2() {

    this.items2.sort(function (a, b) { return a.time - b.time });
    this.items2.reverse()

  }
  doRefresh(refresher) {
    console.log('Begin async operation', refresher);
    this.number = this.number + 6
    setTimeout(() => {
      this.load()
      refresher.complete();
    }, 2000);
  }

  decode(time) {
    var d = new Date(time);
    var date = d.getDate();
    var Month = d.getMonth();
    var year = d.getFullYear();

    var hr = d.getHours();
    var min = d.getMinutes();
    var sec = d.getSeconds()
    var thmonth = new Array("มกราคม", "กุมภาพันธ์", "มีนาคม",
      "เมษายน", "พฤษภาคม", "มิถุนายน", "กรกฎาคม", "สิงหาคม", "กันยายน",
      "ตุลาคม", "พฤศจิกายน", "ธันวาคม");
    return date + " " + thmonth[Number(Month)] + " " + Number(year + 543);
  }
  min(data) {
    if (data < 10) {
      return "0" + data;
    } else {
      return data
    }
  }
  ref(refresher){
    console.log('Begin async operation', refresher);
    this.Infi_load=false
    setTimeout(() => {
      this.firebaseProvider.getscholarship(6)
      .subscribe(snapshot => {
        this.items = snapshot.reverse()
        this.firebaseProvider.stopLoading()
      });
      refresher.complete();
    }, 2000);

  }
}
