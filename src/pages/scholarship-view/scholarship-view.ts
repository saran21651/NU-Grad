import { Component } from '@angular/core';
import { NavController, NavParams,ViewController,LoadingController,Loading} from 'ionic-angular';

import { SocialSharing } from '@ionic-native/social-sharing';
/**
 * Generated class for the ScholarshipViewPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-scholarship-view',
  templateUrl: 'scholarship-view.html',
})
export class ScholarshipViewPage {
  loading:Loading;
  public ElementObject:any


  constructor(public navCtrl: NavController, public navParams: NavParams,public viewCtrl:ViewController,public socialSharing:SocialSharing,  public loadingCtrl: LoadingController ) {
    this.ElementObject=this.navParams.get('object');
    console.log(this.ElementObject)
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ScholarshipViewPage');
  
  }
  dismiss() {
    this.viewCtrl.dismiss();
  }
  ScholarshipShare(item){
    
      this.socialSharing.share(item.des,item.title,null,item.imgUrl).then(x=>{
        console.log(x)
      }).catch(err=>{
  
      });
  
    
  }
  showLoading() {
    this.loading = this.loadingCtrl.create({
        spinner: 'hide',
        content: `
        <div class="cssload-container" name="bubbles">
            <div class="cssload-whirlpool"></div>
            <img src="assets/ui/load.png" alt="" width="42">
        </div>
        `,
     
        cssClass: 'loader',
        showBackdrop: true ,
        dismissOnPageChange:false

    });
    this.loading.present();

  }

  decode(time) {
    var d = new Date(time);
    var date = d.getDate();
    var Month = d.getMonth();
    var year = d.getFullYear();

    var hr = d.getHours();
    var min = d.getMinutes();
    var sec = d.getSeconds()
    var thmonth = new Array ("มกราคม","กุมภาพันธ์","มีนาคม",
    "เมษายน","พฤษภาคม","มิถุนายน", "กรกฎาคม","สิงหาคม","กันยายน",
    "ตุลาคม","พฤศจิกายน","ธันวาคม");
    return date + " " + thmonth[Number(Month)] + " " + Number(year+543) 
  }
  min(data){
    if(data<10){
      return "0"+data;
    }else{
      return data
    }
  }
}
