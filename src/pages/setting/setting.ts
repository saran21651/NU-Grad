import { Component } from '@angular/core';
import { App, NavController, NavParams, ViewController, Loading, LoadingController, AlertController, Platform, ModalController } from 'ionic-angular';

import { OneSignal } from '@ionic-native/onesignal';
import { FirebaseProvider } from './../../providers/firebase/firebase';
import { LoginPage } from '../login/login';
import { InterviewsPage } from "../interviews/interviews"
import { ReportPage } from '../report/report';
import { ModalcalendarPage } from '../modal-calendar/modal-calendar';
/**
 * Generated class for the SettingPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-setting',
  templateUrl: 'setting.html',
})
export class SettingPage {
  items = [{
    'title': 'แจ้งเตือนทั่วไป',
    'modal': 'notification',
    'img': 'assets/ui/home/notification.png',
    'model': ''
  }, {
    'title': 'แจ้งเตือนทุนการศึกษา',
    'modal': 'scholarship',
    'img': 'assets/ui/home/notification.png',
    'model': ''
  }, {
    'title': 'แจ้งเตือนกิจกรรม',
    'modal': 'Event',
    'img': 'assets/ui/home/notification.png',
    'model': ''
  }]

  loading: Loading;
  toggleSubscribe1: boolean;

  toggleSubscribe2: boolean;
  tagsSub: any;
  isDataAvailable = false;
  noti: any;
  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public viewCtrl: ViewController
    , public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    public oneSignal: OneSignal,
    public firebaseProvider: FirebaseProvider,
    public app: App,
    public platform: Platform,
    public modalCtrl: ModalController
  ) {
    this.firebaseProvider.showLoading();
    this.firebaseProvider.stopLoading();
  }

  ionViewDidLoad() {
    let i
    for (i = 0; i < this.items.length; i++) {

      this.items[i].model = localStorage.getItem('setting' + i)

    }
  }


  dismiss() {
    this.viewCtrl.dismiss();
  }
  logout() {


    this.presentConfirm()

  }
  presentConfirm() {

    let alert = this.alertCtrl.create({
      title: 'ออกจากระบบ',
      message: 'คุณต้องการจะออกใช่หรือไม่',
      buttons: [
        {
          text: 'ยกเลิก',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');

          }
        },
        {
          text: 'ตกลง',
          handler: () => {
            localStorage.removeItem('UserId');
            localStorage.removeItem('Token');

            location.reload()
            // this.platform.exitApp();

          }
        }
      ]
    });
    alert.present();


  }


  save() {
    let i
    let alert = this.alertCtrl.create({
      title: 'บันทึการตั้งค่า',
      message: 'ยืนยันบันทึกการตั้งค่าของท่าน',
      buttons: [
        {
          text: 'ยกเลิก',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');

          }
        },
        {
          text: 'ยืนยัน',
          handler: () => {
            console.log('Buy clicked');
            for (i = 0; i < this.items.length; i++) {
              localStorage.setItem(this.items[i].title, this.items[i].model)
              this.oneSignal.startInit("123485eb-918f-4b28-b1cf-655c9355d386")
              this.oneSignal.setSubscription(true)
              this.oneSignal.inFocusDisplaying(this.oneSignal.OSInFocusDisplayOption.Notification)
              this.oneSignal.handleNotificationReceived().subscribe((res) => {

                this.firebaseProvider.setNotiStatus();
                this.noti = JSON.stringify(res);
              })
              this.oneSignal.handleNotificationOpened().subscribe((res) => {
                console.log(res);
                // do something when a notification is opened
              });
              this.oneSignal.getTags()
                .then(res => {
                  this.tagsSub = res
                })
              // this.oneSignal.sendTag("GradNu", this.items[i].model)
              this.oneSignal.endInit()

              this.oneSignal.sendTag(this.items[i].title, this.items[i].model)
              //  this.subscribe2();

              localStorage.setItem('setting' + [i], this.items[i].model)
            }
          }
        }
      ]
    });
    alert.present();
  }


  showLoading() {
    this.loading = this.loadingCtrl.create({
      spinner: 'hide',
      content: `
        <div class="cssload-container" name="bubbles">
            <div class="cssload-whirlpool"></div>

            <img src="assets/ui/load.png" alt="" width="42">
        </div>
        `,
      cssClass: 'loader',
      showBackdrop: true,
      dismissOnPageChange: false
    });
    this.loading.present();
  }

  tools() {
    this.navCtrl.push(InterviewsPage, { 'setting': true })
  }
  report() {

    this.modalCtrl.create(ReportPage).present()
  }
}
