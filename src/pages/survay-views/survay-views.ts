import { Component, PipeTransform, Pipe } from '@angular/core';
import { NavController, NavParams, AlertController, ViewController, ToastController } from 'ionic-angular';
declare var $: any;
import { AngularFireDatabase, FirebaseListObservable } from 'angularfire2/database';
import * as firebase from 'firebase/app';
import { WheelSelector } from '@ionic-native/wheel-selector';
import { FirebaseProvider } from './../../providers/firebase/firebase';
import { Vibration } from '@ionic-native/vibration';
@Component({
  selector: 'page-survay-views',
  templateUrl: 'survay-views.html',
})
export class SurvayViewsPage {
  disableBtn;
  bools = [false];
  surveys: FirebaseListObservable<any[]>;
  survey: any[];
  general = {};
  surveyLength;
  resultLength;
  downloadCer = false;
  title;
  Ishow: any;
  Image: any;
  link: any;
  checkIn: any;
  uid = localStorage.getItem('UserId');
  dummyJson: any;
  name=this.navParams.get('title')
  
  constructor(public navCtrl: NavController, public navParams: NavParams, public alertCtrl: AlertController, public afd: AngularFireDatabase, public view: ViewController,
    public db: FirebaseProvider,
    public selector: WheelSelector,
    public toastCtrl: ToastController,
    public vibration: Vibration
  ) {
    this.check_in()
    this.setcer()
    this.db.showLoading()
    this.title = this.navParams.get('ele')
    console.log(this.title)
    this.afd.list("survey/" + this.title + "/index", {
      query: {
        orderByChild: 'id'
      }
    }).subscribe(res => {
      if (res == null || res == undefined || res.length == 0) {
        this.Ishow = false
      } else {
        console.log(res)
        this.survey = res;
        this.surveyLength = res.length;
        // console.log(this.survey);
        this.Ishow = true
      }
    })
    this.disableBtn = new Array;
    for (let i = 0; i < this.surveyLength; i++) {
      this.disableBtn.push('');
    }

    this.afd.list('survey_result/' + this.navParams.get('ele') + '/' + localStorage.getItem('UserId'), {
      query: {
        orderByChild: 'id',
      }
    }).subscribe(resp => {
      console.log(resp)
      if (resp != null) {
        console.log(resp.length);
        this.resultLength = resp.length;
        resp.forEach(data => {
          this.disableBtn[data.topic.id - 1] = (data.topic.id);
        })

      } else {

      }
      console.log(this.disableBtn);
      if (this.surveyLength == this.resultLength) {
        this.downloadCer = true;
      }
    })
    this.db.stopLoading()
  }
  show(id) {
    this.bools[id] = !this.bools[id];
  }

  GenSend(key, point, id) {
    console.log(key )
    let alert = this.alertCtrl.create({
      title: 'บันทึกข้อมูลสำเร็จ!',
      buttons: ['OK'],
    });
    let confirm = this.alertCtrl.create({
      title: 'ยืนยันการบันทึกข้อมูล',
      message: 'หากบันทึกข้อมูล จะไม่สามารถกลับมาแก้ไขได้อีก',
      buttons: [
        {
          text: 'ยกเลิก',
          handler: () => {
            console.log('Disagree clicked');
          }
        },
        {
          text: 'ยืนยัน',
          handler: () => {
            const itemRef2 = this.afd.list('survey_result/'
              + this.navParams.get('ele') + '/'
              + localStorage.getItem('UserId'))

            const itemRef3 = this.afd.list('survey_show/' + this.navParams.get('ele') + '/')


            var d = new Date();
            var n = d.getTime();
            this.show(id);

            point.time = n;
            point.topic = key;
            console.log(point)
            itemRef2.push(point)
            itemRef3.push(point)
              // this.view.dismiss()
              .then(() => alert.present());

          }
        }
      ]
    });
    confirm.present();
  }
  dismiss() {
    this.view.dismiss()
  }
  openCer() {
    window.location.href = this.link
    console.log("test");
  }
  setcer() {
    this.afd.object('survey/' + this.navParams.get('ele') + "/status")
      .subscribe(res => {
        this.Image = res.img
        this.link = res.cer
      })
  }
  check_in() {
    this.afd.object('CheckIn/' + this.navParams.get('ele')).subscribe(snap => {
      this.checkIn = snap[localStorage.getItem('UserId')]
    })
  }

  openPicker(item,id,point) {
  
  
    console.log(point,id) 

    var array = $.map(item, function (value, index) {
      return [value];
    });
  
   var json={
            data:array
   };

      this.selector.show({
        title: 'เลือกคำตอบของคุณ',
        items: [
          json['data']
        ],
        displayKey: 'name',
        positiveButtonText: "เลือก",
        negativeButtonText: "ยกเลิก",
      }).then(
        result => {
          // let msg = `เลือก ${result[0].name}`;
          // let toast = this.toastCtrl.create({
          //   message: msg,
          //   duration: 4000
          // });
          // toast.present();
          
          this.survey[point].choice[id]=result[0].name;
        },
        err => console.log('Error: ', err)
        );
  }

  viba(){
    this.vibration.vibrate(10);

  }
}
