import {
  NavController,
  NavParams,
  AlertController,
  ModalController,
  Item,
  ViewController
} from 'ionic-angular';
import {
  Component,
  PipeTransform,
  Pipe
} from '@angular/core';
import {
  SurvayViewsPage
} from '../survay-views/survay-views';
import {
  AngularFireDatabase,
  FirebaseListObservable
} from 'angularfire2/database';
import * as firebase from 'firebase/app';

import { FirebaseProvider } from './../../providers/firebase/firebase';
@Component({
  selector: 'page-survay',
  templateUrl: 'survay.html',
})
export class SurvayPage {
  event = [];
  buffer = []
  item = {}
  item2 = {}
  statusview = 1;
  result: any;
  Ishow: any;
  checkIn: any;
  checkif = 0;
  event_result: any
  constructor(public navCtrl: NavController, public navParams: NavParams, public alertCtrl: AlertController,
    public afd: AngularFireDatabase,
    public modalCtrl: ModalController,
    public viewCtrl: ViewController,
    public db: FirebaseProvider
  ) {
    this.checkEvent()
    this.loadevent();
    this.Survay_event()
    setTimeout(() => {
      this.db.stopLoading()
    }, 3000);
  }
  ngOnInit() {

  }
  checkSur(item) {

    var object = this.checkIn[item];
    if (object[localStorage.getItem('UserId')]) {
      return true
    } else {
      return undefined
    }
  }
  checkEvent() {
    this.db.showLoading()
    this.afd.list('/User_event/' + localStorage.getItem('UserId')).subscribe(snap => {

      snap.forEach(ele => {

        this.afd.object('popup/' + ele.$key).subscribe(Isnapsho => {
          this.afd.list('/survey_result/' + Isnapsho.key + '/' + localStorage.getItem('UserId'))
            .subscribe(result => {

              if (result.length == 0 && this.statusview == 1 && this.checkif == 0) {

                this.afd.object('/popup/' + ele.$key)
                  .subscribe(snap => {
                    this.item['time'] = snap.timeStart;
                    this.item['title'] = snap.key;
                    this.item['key'] = snap.title;
                    this.item['status'] = 0;
                    this.event.push(this.item)
                    this.item = {}
                  })

              } else if (this.statusview == 1) {
                this.afd.list('survey/' + Isnapsho.key + '/index').subscribe(result2 => {
                  this.afd.object('/popup/' + ele.$key)
                    .subscribe(snap => {
                      if (result.length == result2.length) {
                        this.item2['time'] = snap.timeStart
                        this.item2['title'] = snap.key
                        this.item2['key'] = snap.title;
                        this.item2['status'] = 1
                        this.event.push(this.item2)
                        this.item2 = {}
                      } else {
                        this.item2['time'] = snap.timeStart
                        this.item2['title'] = snap.key
                        this.item2['status'] = 0
                        this.item2['key'] = snap.title;
                        this.event.push(this.item2)
                        this.item2 = {}
                      }
                    });
                });
              }
              this.db.stopLoading()
            });
        })
      });
    });
  }
  loadevent() {
    this.afd.object('CheckIn/').subscribe(snap => {
      this.checkIn = snap;  
    });
  }
  viewSurway(item, item2) {
    this.checkif = 0
    console.log(item)
    this.statusview = 0;
    let madal = this.modalCtrl.create(SurvayViewsPage, { 'ele': item, 'title': item2 })
    madal.present()
    madal.onDidDismiss(data => {
      this.event = [];
      this.statusview = 1;
      this.checkEvent()

    });
  }
  dismiss() {
    this.viewCtrl.dismiss()
  }
  doRefresh(refresher) {
    console.log('Begin async operation', refresher);

    setTimeout(() => {
      // this.checkEvent()
      refresher.complete();
    }, 2000);
  }

  decode(time) {
    var d = new Date(time);
    var date = d.getDate();
    var Month = d.getMonth();
    var year = d.getFullYear();

    var hr = d.getHours();
    var min = d.getMinutes();
    var sec = d.getSeconds()
    var thmonth = new Array("มกราคม", "กุมภาพันธ์", "มีนาคม",
      "เมษายน", "พฤษภาคม", "มิถุนายน", "กรกฎาคม", "สิงหาคม", "กันยายน",
      "ตุลาคม", "พฤศจิกายน", "ธันวาคม");
    return date + " " + thmonth[Number(Month)] + " " + Number(year + 543)
  }
  min(data) {
    if (data < 10) {
      return "0" + data;
    } else {
      return data
    }
  }

  Survay_event() {
    this.afd.object('survey/').subscribe(x=>{
      this.event_result = x
      console.log(this.event_result)
    })
  
  }
  survay_cheack(key) {
    if(this.event_result[key]){
      return true
    }else{
      return false
    }

  }
}
