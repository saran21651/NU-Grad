import { Component } from '@angular/core';
import { NavController, NavParams, ViewController } from 'ionic-angular';
import { FirebaseProvider } from './../../providers/firebase/firebase';
import { Platform } from 'ionic-angular/platform/platform';
import { Http, Headers, RequestOptions } from '@angular/http'
/**
 * Generated class for the ThesisPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
declare var jquery: any;
declare var $: any;

@Component({
  selector: 'page-thesis',
  templateUrl: 'thesis.html',
})
export class ThesisPage {
  items: any
  value = this.navParams.get('sum')
  type: any = this.navParams.get('type')
  viewStatus = 0;
  viewStatus2 = 0;
  percent: any = 0;
  engStatus = 0;
  advisorStatus = 0;
  researchHuman: any;
  count1: any = 0;
  count2: any = 0;
  result: any = 0;
  qualification = 0;
  PermitInfo = 0;
  publication = 0;
  thesisexam = 0;
  getCompleteInfo_value = 0;

  constructor(public navCtrl: NavController
    , public navParams: NavParams,
    public viewCtrl: ViewController,
    public db: FirebaseProvider,
    public platform: Platform,
    public http: Http
  ) {
    // this.db.showLoading()
    // this.items= this.navParams.get('ele')
    // console.log(this.items)
    // this.setlenght(this.type)
    // this.db.stopLoading()
  }
  ionViewDidLoad() {
    this.setstatus()
  }
  setstatus() {
   
    this.db.showLoading();
    let urlApi = 'http://www.db.grad.nu.ac.th/apps/ws/get_program/' + localStorage.getItem('types')
    let headers = new Headers({ 'Content-Type': 'application/json', "Authorization": 'JWT ' + localStorage.getItem('Token') });
    let option = new RequestOptions({ headers: headers });
    this.http.post(urlApi, null, option).map(x => x.json()).subscribe(data => {
      var type = data.result[0].PROGRAMVERSION_N
      var status = type
      var text = data.result[0].PROGRAMNAMECERTIFY

      if (status == "แผน ก  แบบ ก 1" ||
        status == "แผน ก  แบบ ก 2") {

          this.type=1
          this.Publication(1);//9urb,rB
          this.ThesisExam(1);//สอบจบ
          this.getPermitInfo(1);
          this.getCompleteInfo(1);
          this.advisorChecklist(1);//ที่ปรักษา
          this.menusetting(1);//ภาษาอังกฤษ
          this.setlenght(1)
     
      } else if (status == "แผน ข") {
      
          this.type=2
          this.Publication(2);
          this.advisorChecklist(2);
          this.menusetting(2);
          this.setlenght(2)
   
      }
      else if (status == "แบบ 1.1"
        || status == "แบบ 1.2"
        || status == "แบบ 2.1"
        || status == "แบบ 2.2"
      ) {


          this.type=0
          this.Qualification(0)
          this.Publication(0);
          this.ThesisExam(0);

          this.getPermitInfo(0);

          this.getCompleteInfo(0);
          this.advisorChecklist(0);
          this.menusetting(0);
          this.setlenght(0)
        
  


      }

    }, error => {
      console.log(error);
      this.db.stopLoading();
    });
  }
  dismiss() {
    this.viewCtrl.dismiss()
  }
  setlenght(index) {


    if (index == 0) {
      this.items = {
        'Thesis_Exam': this.getCompleteInfo_value,//สอบจบ
        'English_Testing': this.engStatus,//สอบ อังกฤษ
        'Advisor_Assignment': this.advisorStatus,//แต่งตั้งที่ปรึกษา
        'Thesis_Complete': this.thesisexam,//ส่งเล่มสมบูรณ์
        'Qualification': this.qualification,//ผ่านการสอบวัดคุณสมบัติ
        'Publication': this.publication ,//ตรพิมพ์
        'Thesis_Permit': this.PermitInfo//อนุมัติทำวิจัย
      }
  
    } else if (index == 1) {

      this.items = {

        'Thesis_Exam': this.getCompleteInfo_value,//สอบจบ
        'English_Testing': this.engStatus,//สอบ อังกฤษ
        'Advisor_Assignment': this.advisorStatus,//แต่งตั้งที่ปรึกษา
        'Thesis_Complete': this.thesisexam,//ส่งเล่มสมบูรณ์
        'Publication': this.publication ,//ตรพิมพ์
        'Thesis_Permit': this.PermitInfo//อนุมัติทำวิจัย
      }

    

    } else if (index == 2) {
      this.items = {
        'English_Testing': this.engStatus,//สอบ อังกฤษ
        'Advisor_Assignment': this.advisorStatus,//แต่งตั้งที่ปรึกษา
        'Publication': this.publication ,//ตรพิมพ์
      }
      console.log(this.items)
  
    }
  }

  Qualification(type) {
    let urlApi = 'http://www.db.grad.nu.ac.th/apps/ws/getQEInfo/' + localStorage.getItem('UserId')
    let headers = new Headers({ 'Content-Type': 'application/json', "Authorization": 'JWT ' + localStorage.getItem('Token') });
    let option = new RequestOptions({ headers: headers });
    this.http.post(urlApi, null, option)
      .map(x => x.json())
      .subscribe(data => {
        if (data.result[0].result) {
          
          this.qualification = 1;

        }

      }, error => {
        console.log(error);

      });
  }

  ThesisExam(type) {
    let urlApi = 'http://www.db.grad.nu.ac.th/apps/ws/getExamInfo/' + localStorage.getItem('UserId')
    let headers = new Headers({ 'Content-Type': 'application/json', "Authorization": 'JWT ' + localStorage.getItem('Token') });
    let option = new RequestOptions({ headers: headers });
    this.http.post(urlApi, null, option)
      .map(x => x.json())
      .subscribe(data => {
        if (data.result[0].exam_date) {
          this.thesisexam = 1

        }
      }, error => {
        console.log(error);


      });
  }
  Publication(type) {
    let urlApi = 'http://www.db.grad.nu.ac.th/apps/ws/getPublicationInfo/' + localStorage.getItem('UserId')
    let headers = new Headers({ 'Content-Type': 'application/json', "Authorization": 'JWT ' + localStorage.getItem('Token') });
    let option = new RequestOptions({ headers: headers });
    this.http.post(urlApi, null, option)
      .map(x => x.json())
      .subscribe(data => {
        if (data.result[0].publication_date) {
          this.publication = 1;

        }
      }, error => {
        console.log(error);

      });
  }
  getPermitInfo(type) {
    let urlApi = 'http://www.db.grad.nu.ac.th/apps/ws/getPermitInfo/' + localStorage.getItem('UserId')
    let headers = new Headers({ 'Content-Type': 'application/json', "Authorization": 'JWT ' + localStorage.getItem('Token') });
    let option = new RequestOptions({ headers: headers });
    this.http.post(urlApi, null, option)
      .map(x => x.json())
      .subscribe(data => {
        if (data.result[0].permit_date) {
          this.PermitInfo = 1;
   
        }
      }, error => {
        console.log(error);

      });
  }
  getCompleteInfo(type) {
    let urlApi = 'http://www.db.grad.nu.ac.th/apps/ws/getCompleteInfo/' + localStorage.getItem('UserId')
    let headers = new Headers({ 'Content-Type': 'application/json', "Authorization": 'JWT ' + localStorage.getItem('Token') });
    let option = new RequestOptions({ headers: headers });
    this.http.post(urlApi, null, option)
      .map(x => x.json())
      .subscribe(data => {

        if (data.result[0].thesiscomplete_date) {
          this.getCompleteInfo_value = 1;
        }
      }, error => {
        console.log(error);


      });

  }
  advisorChecklist(type) {
    let headers = new Headers({
      'Content-Type': 'application/json',
      "Authorization": 'JWT ' + localStorage.getItem('Token')
    });

    let option = new RequestOptions({
      headers: headers
    });
    let urlApi = 'http://www.db.grad.nu.ac.th/apps/ws/getAdvisorAssignStudInfo/' + localStorage.getItem('UserId')
    this.http.post(urlApi, null, option).map(x => x.json()).subscribe(data => {
      let advisorUrl = 'http://www.db.grad.nu.ac.th/apps/ws/getGradStaff/' + data.result[0].advisor_id
      this.http.post(advisorUrl, null, option)
        .map(x => x.json())
        .subscribe(snapshot => {
          this.db.stopLoading();
          if (snapshot.error != null || snapshot.error != undefined) {
            this.advisorStatus = 0
          } else {
            this.advisorStatus = 1
  
          }
        })
    });
  }
  menusetting(type) {
    let headers = new Headers({
      'Content-Type': 'application/json',
      "Authorization": 'JWT ' + localStorage.getItem('Token')
    });
    let option = new RequestOptions({
      headers: headers
    });
    let urlApi = 'http://www.db.grad.nu.ac.th/apps/ws/getEnglishInfo/' + localStorage.getItem('UserId')
    this.http.post(urlApi, null, option).map(x => x.json()).subscribe(data => {
      if (data.result[0] != undefined) {
        if (data.result[0].pass_test == true) {
          this.engStatus = 1
  
        } else {
          this.engStatus = 0
        }

      } else {

      }

    })
  }
}


