import { Component } from '@angular/core';
import { NavController, NavParams, ViewController } from 'ionic-angular';

declare let ESPxPlayer: any;

@Component({
  selector: 'page-video-view',
  templateUrl: 'video-view.html',
})
export class VideoViewPage {

  programID = this.navParams.get('programID');

  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController) {
    console.log(this.programID);
    this.createPlayer();
  }

  createPlayer(){
    var options = {
      authorizationToken:'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6ImU1NjkxNTE5LTFmZmEtNDdjYS04MDUwLTYzOGVlNzc5MzdjYiIsInJvbGVzIjpbeyJpZCI6ImIzMDdiNzkyLWUzNzctNDY1Mi1iMjYyLTUzMzUzZWY1MzQzNyIsIm5hbWUiOiJhZG1pbiJ9XSwiaWF0IjoxNTI1NjYyODgxLCJleHAiOjE1MjU3NDkyODEsImlzcyI6IkVTUHhNZWRpYSJ9.RmaiUR4PJfFtuoZxqf32T9_loXqcGNiLNTaUu6IFEa4',
      divID: '#container1',
      autoPlay: true,
      config: "espxplayer",
      programme_id: this.programID.id,
      chatConfiguration: {
        uuid: 'e5691519-1ffa-47ca-8050-638ee77937cb'
      }
    }
    ESPxPlayer.createPlayer(options);

  }
  ionViewWillLeave(){
    ESPxPlayer.destroyPlayer();
  }

  closeVideoPlayer(){
    ESPxPlayer.destroyPlayer();
  }

  dismiss() {
    this.viewCtrl.dismiss()
  }

}
