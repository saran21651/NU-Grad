import { Component } from '@angular/core';
import { AlertController, NavController, ViewController, ModalController } from 'ionic-angular';
import { Http } from '@angular/http';

declare let ESPxPlayer: any;

import { CoreService } from '../../services/core-service';
import { ENV } from "../../config";
import { VideoViewPage } from '../video-view/video-view';
declare var jquery: any;
declare var $: any;
import * as firebase from 'firebase';
import {FirebaseProvider} from './../../providers/firebase/firebase';
/**
 * Generated class for the VideoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-video',
  templateUrl: 'video.html',
})
export class VideoPage {
  programID: any;
  items: any = [{ "image": "" }];
  constructor(public navCtrl: NavController,
    public coreSvc: CoreService,
    public alertCtrl: AlertController,
    public viewCtrl: ViewController,
    public http: Http,
    public modalCtrl: ModalController,
    public db:FirebaseProvider
  ) {
    this.db.showLoading()

    firebase.database().ref('live_key').on('value',snapshot=>{
      this.http.get('https://api.espx.cloud/api/v1/events/'+snapshot.val().key).map(res => res.json()).subscribe(data => {
        console.log(data)
        // console.log(data.programmes);
        this.programID = data.programmes;
        this.db.stopLoading()
      });
      console.log('loadding')
    });
    setTimeout(() => {
      this.db.stopLoading()
    },5000);
    this.load_news()
  }

  watchVideo(item) {
    this.navCtrl.push(VideoViewPage, { 'programID': item });

  }
  decode(time) {
    var d = new Date(time);
    var date = d.getDate();
    var Month = d.getMonth();
    var year = d.getFullYear();

    var hr = d.getHours();
    var min = d.getMinutes();
    var sec = d.getSeconds()
    var thmonth = new Array("มกราคม", "กุมภาพันธ์", "มีนาคม",
      "เมษายน", "พฤษภาคม", "มิถุนายน", "กรกฎาคม", "สิงหาคม", "กันยายน",
      "ตุลาคม", "พฤศจิกายน", "ธันวาคม");
    return date + " " + thmonth[Number(Month)] + " " + Number(year + 543)
  }
  min(data) {
    if (data < 10) {
      return "0" + data;
    } else {
      return data
    }
  }
  dismiss() {
    this.viewCtrl.dismiss()
  }
  load_news() {
    firebase.database().ref('live_news').limitToLast(7).once('value', snapshot => {
      this.items = $.map(snapshot.val(), function (value, index) {
        return [value];
      });
      this.Sort()
    });


  }

    Sort() {

    this.items.sort(function (a, b) { return a.time - b.time });
    this.items.reverse()

  }
}
