import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database';
import * as firebase from 'firebase';
import { ViewController, Loading, LoadingController, AlertController } from 'ionic-angular';
import { Calendar } from '@ionic-native/calendar';

@Injectable()

export class FirebaseProvider {

  constructor(public afd: AngularFireDatabase,
    public loadingCtrl: LoadingController,
    public calendar: Calendar,
    public alertCtrl: AlertController
  ) { }

  loading: Loading

  getItems() {
    return this.afd.list('persons/notification/management/', {
      query: {
        orderByChild: 'nagativetime'
      }
    })
  }

  getStatus(sid) {
    return this.afd.list('StudentMsg/' + sid)
  }

  updateStatus(msgkey, SId) {
    this.afd.list('StudentMsg/' + SId + '/' + msgkey, { preserveSnapshot: true })
      .subscribe(snapshot => {
        this.afd.object('StudentMsg/' + SId + '/' + msgkey).update({ 'status': '1', 'key': msgkey });
      })
  }

  setNotiStatus() {
    let id = localStorage.getItem('UserId');
    this.afd.list('persons/notification/management/', { preserveSnapshot: true })
      .subscribe(snapshot => {
        snapshot.forEach(snapshot => {
          this.afd.object('StudentMsg/' + id + '/' + snapshot.key).update({ 'status': '0', 'key': snapshot.key });
        })
      })
  }


  addItem(sId, pId, img) {
    if (sId != '' && pId != '') {
      this.afd.list('/IDList/' + sId, { preserveSnapshot: true })
        .subscribe(async snapshot => {
          if (snapshot.length != 0) {
            snapshot.forEach(async snapshot => {
              if (snapshot.key == 'SId' && snapshot.val() == sId) {
                let waitAddpicture = await this.addPicture(sId, img);
                if (waitAddpicture) {
                  this.afd.object('/IDList/' + sId).update({ 'PId': pId });
                  console.log("UPDATE Player ID");
                }
              }
            })
          } else if (snapshot.length == 0) {
            let waitAddpicture = await this.addPicture(sId, img);
            // console.log(waitAddpicture);
            let imgUrl = await this.getUrlImg(sId);
            // console.log(imgUrl);
            if (waitAddpicture) {
              var idTemmp = {
                'SId': sId,
                'PId': pId,
                'ImgUrl': imgUrl
              }
              this.afd.object('/IDList/' + sId).set(idTemmp);
              console.log("ADD");
            }
          }
        })
    } else {
      console.log("Pleass fill data");
    }
  }

  addPicture(sId, image) {
    let storageRef = firebase.storage().ref().child('/images/' + sId);
    // console.log("storageRef");
    return storageRef.put(image[0]).then(snapshot => {
      console.log('successfully added picture');
      return true;
    }).catch(err => { console.error("Whoupsss!", err) })
  }

  getUrlImg(sId) {
    return firebase.storage().ref().child("/images/" + sId).getDownloadURL().then(urlresp => {
      return urlresp
    });

  }

  getInfo(id) {
    let ids, idp, url;

    this.afd.list("/IDList/" + id).subscribe(function (res) {
      res.forEach((resp) => {
        if (resp.$key == 'SId') {
          ids = resp.$value;
        }
        if (resp.$key == 'PId') {
          idp = resp.$value;
        }
        if (resp.$key == 'ImgUrl') {
          url = resp.$value;
        }
      })
    });
    let data = {
      'StudenID': ids,
      'PlayerID': idp,
      'ImageURL': url
    }
    //console.log(data);
    return data;
  }

  getscholarship(no) {
    return this.afd.list('/scholarship/', {
      query: {
        orderByChild: 'time',
        limitToLast: no,
      }
    });

  }
  getnotification() {
    return this.afd.list('/notification/', {
      query: {
        orderByChild: 'time',
        limitToLast: 25,
      }
    }
    )

  }
  ResultNotification() {

    return this.afd.list('/persons/notification/' + localStorage.getItem('UserId'), {
      query: {
        orderByChild: 'time',
        limitToLast: 25,

      }
    })
  }
  homescolarship() {
    return this.afd.list('/scholarship/', {
      query: {
        orderByChild: 'time',
        limitToLast: 5,
      }
    });
  }

  readnotification(data, Userid) {

    this.afd.object('/persons/notification/' + Userid + "/" + data.$key + '/read/' + Userid).set({
      status: 'read'
    })
  }
  readnotificationAll(data, Userid) {
    this.afd.object('/notification/' + data.$key + "/read/" + Userid).set({
      status: 'read'
    })
  }
  qurestion() {
    return this.afd.list('/survey')
  }
  popup() {
    return this.afd.list('/popup', {
      query: {
        orderByChild: 'status',
        equalTo: 1,

      },

    })

  }
  announcelist_slide() {
    return this.afd.list('/popup', {
      query: {
        orderByChild: 'type',
        equalTo: 'event',
        limitToLast: 5,
      },
    })

  }
  announcelist() {
    return this.afd.list('/popup', {
      query: {
        orderByChild: 'type',
        equalTo: 'event',
        limitToLast: 20,
      },
    })

  }
  announcelist2(number) {
    return this.afd.list('/popup', {
      query: {
        orderByChild: 'type',
        equalTo: 'News',
        limitToLast: number,
      },
    })
  }
  popup_normal() {

    return this.afd.list('/popup_normal', {
      query: {
        orderByChild: 'status',
        equalTo: 1,
      },
    })

  }
  register(ele, sum) {
    var status;
    var x = []

    this.afd.object('/popup/' + ele.key + '/Attendees/interes')
      .subscribe(snap => {

        x = snap;

      });
    setTimeout(() => {
      var count = 0;
      for (var k in x) {
        if (x.hasOwnProperty(k)) {
          ++count;
        }
      }

      if (count - 1 < sum) {
        this.afd.object('UserInfo/' + localStorage.getItem('UserId')).subscribe(snap => {
          this.afd.object('/popup/' + ele.key + '/Attendees/interes/' + localStorage.getItem('UserId')).set({
            'nuid': localStorage.getItem('UserId'),
            'class': "1",
            'name': snap.THname,
            'surname': snap.THsurname,
            'status': "1",
            "Faculty": snap.Faculty,
            'time': firebase.database.ServerValue.TIMESTAMP,
          })
          this.presentAlert('', 'ลงทะเบียนเรียบร้อย')
          this.saveEvent(ele)
          firebase.database().ref('popup/' + ele.key).once('value', event => {
            this.afd.list('/persons/notification/' + localStorage.getItem('UserId')).push({
              'title': 'ลงทะเบียนเข้าร่วมกิจกรรมเรียบร้อยแล้ว',
              'type': "person",
              'time': firebase.database.ServerValue.TIMESTAMP,
              'key': localStorage.getItem('UserId'),
              'des': 'ท่านได้ลงทะเบียนกิจกรรมเข้าร่วมงาน ' + event.val().title,
              'read': {
                'status': 'read'
              },
    
            })
          })
        })
      } else {
        alert('ขออภัยมีผู้ลงทะเบียนครบจำนวนแล้ว')
      }
    }, 1000);
  }
  myEvect() {
    return this.afd.list('User_event/' + localStorage.getItem('UserId'));
  }
  event(event) {
    let list = [];
    var arr = Object.keys(event)

    arr.forEach(element => {
      this.afd.object('/popup/' + element
      ).subscribe(snap => {
        console.log(snap)
        list.push(snap)
      });
    })
    return list
  }


  showLoading() {
    this.loading = this.loadingCtrl.create({
      spinner: 'hide',
      content: `
          <div class="cssload-container" name="bubbles">
              <div class="cssload-whirlpool"></div>
              <img src="assets/ui/load.png" alt="" width="42">
          </div>
          `,
      cssClass: 'loader',
      showBackdrop: true,
      dismissOnPageChange: false

    });
    this.loading.present();

  }

  internetlost() {
    this.loading = this.loadingCtrl.create({
      spinner: 'hide',
      content: `
          <div class="cssload-container" name="bubbles">
              <div class="cssload-whirlpool"></div>
              <img src="assets/ui/internet.png" alt="" width="42">
          </div>
          `,
      cssClass: 'loader',
      showBackdrop: true,
      dismissOnPageChange: false

    });
    this.loading.present();

  }
  stopLoading() {
    this.loading.dismiss()
  }
  dts(No) {

    return this.afd.list('Dts/' + localStorage.getItem('UserId'), {
      query: {
        orderByChild: 'time',
        limitToLast: No,
      }
    })


  }
  checkin(data) {
    var obj: any = {
      'id': data.STUDENTCODE,
      'THname': data.STUDENTNAME,
      'THsurname': data.STUDENTSURNAME,
      'ENGname': data.STUDENTNAMEENG,
      'ENGsurname': data.STUDENTSURNAMEENG,
      'Faculty': data.PROGRAMNAME
    }


    this.afd.object('UserInfo/' + localStorage.getItem('UserId')).update(obj)


    firebase.database().ref('UserInfo/' + localStorage.getItem('UserId'))
      .once('value', snapshot => {
        console.log(snapshot.val().snapshot)
        if (snapshot.val().first_login == undefined) {

          this.afd.object('UserInfo/' + localStorage.getItem('UserId')).update({
            'first_login': firebase.database.ServerValue.TIMESTAMP
          })
        }
      })
    firebase.database().ref('UserInfo/' + localStorage.getItem('UserId') + '/OnesignalKey').on('value', snap => { });
  }
  updatenotification(key) {
    localStorage.setItem('playerId', key.userId)
    this.afd.object('UserInfo/' + localStorage.getItem('UserId')).update({ 'OnesignalKey': key })
    // this.one_login(key)
  }
  updatePlayerID(key) {
    this.afd.object('UserInfo/' + localStorage.getItem('UserId')).update({
      'OnesignalKey': {
        userId: key
      }
    })
  }
  // one_login(key) {
  //   console.log(key)
  //   firebase.database().ref('UserInfo/' + localStorage.getItem('UserId') + '/OnesignalKey').on('value', snap => {

  //     if (localStorage.getItem('playerId')) {
  //       if (snap.val().userId != localStorage.getItem('playerId')) {
  //         this.logout()

  //       } else {

  //       }
  //     }


  //   })
  // }
  // logout() {

  //   localStorage.removeItem('UserId');
  //   localStorage.removeItem('Token');
  //   location.reload()
  // }
  storage(str1, str2, str3, uid) {
    var storageRef = firebase.storage().ref('FaceID/' + uid + '/').child('front');
    storageRef.putString(str1, 'data_url').then(snapshot => {
      var storageRef2 = firebase.storage().ref('FaceID/' + uid + '/').child('right');
      storageRef2.putString(str2, 'data_url').then(snapshot2 => {
        var storageRef3 = firebase.storage().ref('FaceID/' + uid + '/').child('left');
        storageRef3.putString(str3, 'data_url').then(snapshot3 => {
          this.urlstorage(snapshot.downloadURL, snapshot2.downloadURL, snapshot3.downloadURL)
        });
      });
    });
  }
  urlstorage(data1, data2, data3) {
    var obj = []
    obj['front'] = data1;
    obj['right'] = data2;
    obj['left'] = data3;

    this.afd.object('UserInfo/' + localStorage.getItem('UserId') + '/FaceID').set(obj)
  }
  faceid() {

    return this.afd.object('UserInfo/' + localStorage.getItem('UserId'))
  }
  notificationAlert() {
    return this.afd.list('notification/', {

      query: {
        orderByChild: 'time',
        limitToLast: 25,
      }


    })
  }
  notificationPrivate() {
    return this.afd.list('persons/notification/' + localStorage.getItem('UserId'), {

      query: {
        orderByChild: 'time',
        limitToLast: 25,

      }

    })
  }


  saveEvent(data) {
    console.log(data)
    var dS = new Date(data.time)
    var dE = new Date(data.timeEvent)


    this.calendar.createEvent(data.title,
      data.address,
      data.des,
      dS,
      dE
    ).then(
      (msg) => {

        var d = new Date(data.time + " 00:00:00").getTime()
        var d2 = new Date(data.time + " 07:" + Math.floor((Math.random() * 10) + 1) + ":" + Math.floor((Math.random() * 10) + 1)).getTime()

        this.afd.object('calendar/' + localStorage.getItem('UserId') + '/' + d + "/" + d2).set({
          "address": data.address,
          'des': data.des,
          "end": d2,
          "start": d2,
          "title": data.title
        })

      });
  }
  Ofcalendar() {

    return this.afd.list('calendar/')


  }
  guests_notification2(no) {
    return this.afd.list('popup', {
      query: {
        orderByChild: 'time',
        limitToLast: no,
      }
    })
  }
  guests_notification() {
    return this.afd.list('popup', {
      query: {
        orderByChild: 'type',
        equalTo: 'event',
        limitToLast: 10,
      }
    })
  }
  couse() {
    return this.afd.object('couse')
  }
  interesting(ele_register) {
    this.afd.list('Guest_register').push(ele_register)
  }
  callevent() {
    return firebase.database().ref('calendar/')
  }

  addCalendar() {
    return this.afd
  }



  presentAlert(title, des) {
    let alert = this.alertCtrl.create({
      title: title,
      subTitle: des,
      buttons: ['ตกลง']
    });
    alert.present();
  }

}
