export class UserModel{
  id: string;
  first_name: string;
  last_name: string;
  display_name: string;
  email: string;
  billing_address: string;
  billing_country: string;
  billing_postal_code: string;
  ship_to_billing_addr: boolean;
  archived: boolean;
  created_by: string;
  date_created: Date;
  updated_by: string;
  date_updated: Date;
  roles: Array<Role>;
  profile_pic: string;
  no_of_followers: number;
}
export class Role{
  id: string;
  name: string;
}
