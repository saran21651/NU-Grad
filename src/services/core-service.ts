import {Http, Request, Response, Headers, RequestOptions, RequestMethod} from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { ENV } from '../config';
import { ReplaySubject } from 'rxjs/ReplaySubject';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import {HttpModule} from "@angular/http";

import {UserModel} from "./core-service.model";
import {Injectable} from "@angular/core";

@Injectable()
export class CoreService{
  private _token: string = null;
  private _userId: string = null;
  private _userInfo: UserModel;
  private _authSource = new ReplaySubject<boolean>(0);

  constructor(private http:Http){}

  get token(): string{
    return this._token;
  }

  auth(email, password): Observable<any> {
    return this.doPost('api/v1/auth/local', { email: email, password: password }).map(
      data => {
        localStorage.setItem("espxUser", JSON.stringify(data));
        this._authSource.next(true);
        this._token = data.token;
        this._userId = data.data.id;
        this._userInfo = data.data;
        return data;

      }
    ).catch(this.handleError);
  }

  doPost(path, data?): Observable<any> {
    return this.doRequest(RequestMethod.Post, path, data);
  }

  private doRequest(method, path, data?): Observable<any> {
    let url = ENV.config.CORE_API_PATH + path;

    var headers = new Headers();

    headers.append('Accept', 'application/json');
    headers.append('Content-Type', 'application/json');

    if (this._token) {
      headers.append('Authorization', this._token);
    }

    var body = '';

    if (data) {
      body = JSON.stringify(data);
    }

    let requestOptions = new RequestOptions({
      method: method,
      url:url,
      headers: headers,
      body: body
    })

    return this.http.request(new Request(requestOptions))
      .map(this.extractData)
      .catch(this.handleError);
  }

  private handleError (error: Response | any) {
    let errMsg: string;
    if (error instanceof Response) {

      if (error.status === 401) {
        if(this._authSource)
        {
          this._authSource.next(false);
        }
        this._token = '';
      }

      const body = error.json() || '';
      const err = body.error || JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }

    return Observable.throw(error);
  }

  private extractData(res: Response) {
    let body = res.json();
    return body || { };
  }


}
